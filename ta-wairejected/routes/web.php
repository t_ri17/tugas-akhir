<?php

Route::get('/', 'HomeController@dashboard')->name('dashboard');
Route::get('/category', 'HomeController@category')->name('category');
Route::get('/products', 'HomeController@product')->name('product');
Route::get('/product/{id}', 'HomeController@productDetail')->name('productDetail');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/music', 'HomeController@music')->name('music');
Route::get('/photo', 'HomeController@photo')->name('photo');
Route::get('/vidio', 'HomeController@vidio')->name('vidio');
Route::get('/howtobuy', 'HomeController@howtobuy')->name('howtobuy');
Route::get('/faq', 'HomeController@faq')->name('faq');
Route::get('/contact', 'HomeController@contact')->name('contact');
Route::get('/artikel', 'HomeController@artikel')->name('artikel');

Auth::routes();
Route::group(['middleware' => ['web', 'auth','admin'], 'prefix' => 'admin'], function() {
    Route::get('/', 'AdminController@dashboard')->name('admin.dashboard.index');
    Route::group(['prefix' => 'inbox'], function () {
        Route::get('/index/{p}', 'AdminController@inboxIndex')->name('admin.inbox.index');
        Route::get('/form', 'AdminController@inboxForm')->name('admin.inbox.form');
        Route::get('/get/{p}', 'AdminController@inboxGet')->name('admin.inbox.get');
        Route::post('/post', 'AdminController@inboxPost')->name('admin.inbox.post');
        Route::delete('/delete/{id}', 'AdminController@inboxDelete')->name('admin.inbox.delete');
    });

    Route::group(['prefix' => 'product'], function () {
        Route::get('/', 'AdminController@productIndex')->name('admin.product.index');
        Route::get('/get', 'AdminController@productGet')->name('admin.product.get');
        Route::get('/form', 'AdminController@productForm')->name('admin.product.form');
        Route::get('/edit/{id}', 'AdminController@productEdit')->name('admin.product.edit');
        Route::get('/detail/{id}', 'AdminController@productDetail')->name('admin.product.detail');
        Route::post('/post/', 'AdminController@productPost')->name('admin.product.post');
        Route::post('/update/{id}', 'AdminController@productUpdate')->name('admin.product.update');
        Route::delete('/delete/{id}', 'AdminController@productDelete')->name('admin.product.delete');
    });
    Route::group(['prefix' => 'category'], function () {
        Route::get('/', 'AdminController@categoryIndex')->name('admin.category.index');
        Route::get('/get', 'AdminController@categoryGet')->name('admin.category.get');
        Route::get('/form', 'AdminController@categoryForm')->name('admin.category.form');
        Route::post('/post/', 'AdminController@categoryPost')->name('admin.category.post');
        Route::get('/edit/{id}', 'AdminController@categoryEdit')->name('admin.category.edit');
        Route::post('/update/{id}', 'AdminController@categoryUpdate')->name('admin.category.update');
        Route::delete('/delete/{id}', 'AdminController@categoryDelete')->name('admin.category.delete');
    });
    Route::group(['prefix' => 'user'], function () {
        Route::get('/', 'AdminController@userIndex')->name('admin.user.index');
        Route::get('/get', 'AdminController@userGet')->name('admin.user.get');
        Route::get('/form', 'AdminController@userForm')->name('admin.user.form');
        Route::post('/post', 'AdminController@userPost')->name('admin.user.post');
        Route::get('/edit/{id}', 'AdminController@userEdit')->name('admin.user.edit');
        Route::post('/update/{id}', 'AdminController@userUpdate')->name('admin.user.update');
        Route::delete('/delete/{id}', 'AdminController@userDelete')->name('admin.user.delete');
    });
    Route::group(['prefix' => 'order'], function () {
        Route::get('/', 'AdminController@orderIndex')->name('admin.order.index');
        Route::get('/get', 'AdminController@orderGet')->name('admin.order.get');
        Route::get('/edit/{id}', 'AdminController@orderEdit')->name('admin.order.edit');
        Route::post('/update/{id}', 'AdminController@orderUpdate')->name('admin.order.update');
        Route::delete('/delete/{id}', 'AdminController@orderDelete')->name('admin.order.delete');
        Route::delete('/d/delete/{id}', 'AdminController@orderDetailDelete')->name('admin.orderdetail.delete');
        Route::get('/report', 'AdminController@reportIndex')->name('admin.order.report');
    });

    Route::group(['prefix' => 'music'], function () {
        Route::get('/', 'AdminController@musicIndex')->name('admin.music.index');
        Route::get('/get', 'AdminController@musicGet')->name('admin.music.get');
        Route::get('/form', 'AdminController@musicForm')->name('admin.music.form');
        Route::get('/edit/{id}', 'AdminController@musicEdit')->name('admin.music.edit');
        Route::post('/update/{id}', 'AdminController@musicUpdate')->name('admin.music.update');
        Route::post('/post', 'AdminController@musicPost')->name('admin.music.post');
        Route::delete('/delete/{id}', 'AdminController@musicDelete')->name('admin.music.delete');
    });
    Route::group(['prefix' => 'photo'], function () {
        Route::get('/', 'AdminController@photoIndex')->name('admin.photo.index');
        Route::get('/get', 'AdminController@photoGet')->name('admin.photo.get');
        Route::get('/form', 'AdminController@photoForm')->name('admin.photo.form');
        Route::get('/edit/{id}', 'AdminController@photoEdit')->name('admin.photo.edit');
        Route::post('/update/{id}', 'AdminController@photoUpdate')->name('admin.photo.update');
        Route::post('/post', 'AdminController@photoPost')->name('admin.photo.post');
        Route::delete('/delete/{id}', 'AdminController@photoDelete')->name('admin.photo.delete');
    });
    Route::group(['prefix' => 'artikel'], function () {
        Route::get('/', 'AdminController@artikelIndex')->name('admin.artikel.index');
        Route::get('/get', 'AdminController@artikelGet')->name('admin.artikel.get');
        Route::get('/form', 'AdminController@artikelForm')->name('admin.artikel.form');
        Route::get('/edit/{id}', 'AdminController@artikelEdit')->name('admin.artikel.edit');
        Route::post('/update/{id}', 'AdminController@artikelUpdate')->name('admin.artikel.update');
        Route::post('/post', 'AdminController@artikelPost')->name('admin.artikel.post');
        Route::delete('/delete/{id}', 'AdminController@artikelDelete')->name('admin.artikel.delete');
    });
});

Route::group(['middleware' => ['web','auth']], function () {

    Route::group(['prefix' => 'cart'], function () {
        Route::get('/', 'CartController@index')->name('cart.index');
        Route::any('/store/{id}', 'CartController@store')->name('cart.store');
        Route::delete('/delete/{id}', 'CartController@delete')->name('cart.delete');
        Route::post('/update/{id}', 'CartController@update')->name('cart.update');
        Route::get('/empty', function () {
            Cart::destroy();
        })->name('cart.empty');
    });

    Route::group(['prefix' => 'checkout'], function () {
        Route::get('/', 'HomeController@checkoutIndex')->name('checkout.index');
        Route::post('/store', 'HomeController@checkoutstore')->name('checkout.store');
    });

    Route::group(['prefix' => 'history'], function () {
        Route::get('/', 'HomeController@historyIndex')->name('history.index');
        Route::get('/get', 'HomeController@historyGet')->name('history.get');
        Route::get('/detail/{id}', 'HomeController@historyDetail')->name('history.detail');
        Route::delete('/delete/{id}', 'HomeController@historyDelete')->name('history.delete');
    });

    Route::group(['prefix' => 'inbox'], function () {
        Route::get('/', 'HomeController@inboxIndex')->name('inbox.index');
        Route::get('/get/{p}', 'HomeController@inboxGet')->name('inbox.get');
        Route::get('/form', 'HomeController@inboxForm')->name('inbox.form');
        Route::post('/post', 'HomeController@inboxPost')->name('inbox.post');
        Route::delete('/delete/{id}', 'HomeController@inboxDelete')->name('inbox.delete');

    });
    Route::group(['prefix' => 'about'], function () {

    });

    Route::group(['prefix' => 'music'], function () {

    });

    Route::group(['prefix' => 'photo'], function () {

    });
    Route::group(['prefix' => 'artikel'], function () {
        
    });
    Route::group(['prefix' => 'vidio'], function () {

    });

    Route::group(['prefix' => 'howtobuy'], function () {

    });
    Route::group(['prefix' => 'faq'], function () {

    });
    Route::group(['prefix' => 'contact'], function () {

    });});
