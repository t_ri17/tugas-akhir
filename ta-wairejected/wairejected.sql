-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 26, 2019 at 08:49 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wairejected`
--

-- --------------------------------------------------------

--
-- Table structure for table `artikel`
--

CREATE TABLE `artikel` (
  `id_artikel` int(200) NOT NULL,
  `artikel_title` varchar(200) NOT NULL,
  `artikel_image` varchar(200) NOT NULL,
  `artikel_des` varchar(200) NOT NULL,
  `artikel_url` varchar(200) NOT NULL,
  `user_id` int(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `artikel`
--

INSERT INTO `artikel` (`id_artikel`, `artikel_title`, `artikel_image`, `artikel_des`, `artikel_url`, `user_id`) VALUES
(9, 'Wai Rejected Mengeluarkan Silnge Luna', 'artikels/6276df025ddbe086bcc188b4c978a29a.jpg', 'Wai Rejected Akhirnya mengeluarkan sinle Kedua yang berjudul Luna. yang sudah di tunggu-tunggu oleh penggemar Wai Rejected', 'https://pontianak.tribunnews.com/2018/01/22/keren-wai-rejected-luncurkan-single-baru-berjudul-luna', 0),
(10, 'Wai Rejected Rilis Singel Kedua', 'artikels/d99afbe11461942179ddb37b64bf0273.jpg', 'SEJAK dikontrak oleh label rekaman Seven Music kemudian merilis single Why, band jebolan Meet The Labels tahun 2015 asal Pontianak ini mulai dikenal oleh banyak penikmat musik.', 'https://www.pontianakpost.co.id/wai-rejected-band-pontianak-rilis-single-kedua', 0),
(11, 'Wai Rejected Mengeluarkan Album Perdana Mereka \"Terbitlah Terang\"', 'artikels/5a87628b8904c39d4ac2f57ae9ba7b9a.jpg', 'Wai Rejected Mengeluarkan Album Perdana Mereka \"Terbitlah Terang\". Album ini sudah di nanti-nantikan oleh penggemar Band Wai Rejected yang ada di Kalimantan maupun diluar Kalimantan.', 'http://pontinesia.com/berita/wai-rejected-rilis-album-perdana-terbitlah-terang', 0),
(12, 'Wai Rejected, Pemutar Rock Alternatif', 'artikels/f0b1b2759640d6d27109923f50314c57.jpg', 'Salah satunya single dari Band Wai Rejected  “rewind” yang saya dapati dari album “Terbitlah Terang”, koleksi unit rock alternatif potensial, Wai Rejected', 'https://seluang.id/2018/10/28/wai-rejected-pemutar-rock-alternatif/', 0);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`, `created_at`, `updated_at`) VALUES
(1, 'CD', NULL, NULL),
(9, 'BACKPACK', NULL, NULL),
(10, 'PANTS', NULL, NULL),
(11, 'HATS', NULL, NULL),
(12, 'T-Shirt', NULL, NULL),
(13, 'JACKET', NULL, NULL),
(14, 'ACCESSORIES', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `inboxes`
--

CREATE TABLE `inboxes` (
  `inbox_id` int(10) UNSIGNED NOT NULL,
  `inbox_from_id` int(11) NOT NULL,
  `inbox_to_id` int(11) NOT NULL,
  `inbox_text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `inboxes`
--

INSERT INTO `inboxes` (`inbox_id`, `inbox_from_id`, `inbox_to_id`, `inbox_text`, `created_at`, `updated_at`) VALUES
(4, 1, 4, 'test test', NULL, NULL),
(14, 1, 2, 'test user', NULL, NULL),
(17, 9, 5, 'haloooo ajsdkjashd', NULL, NULL),
(18, 5, 9, 'dawdawdawd', NULL, NULL),
(19, 9, 10, 'halooooo wkwjawjjwajwa', NULL, NULL),
(20, 10, 9, 'ahsdchgasgdhasdjknasbdjaks', NULL, NULL),
(21, 10, 9, 'min sudah saya transfer', NULL, NULL),
(22, 9, 10, 'oke saya akan kirim', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_08_08_172808_create_inboxes_table', 1),
(4, '2018_08_08_172941_create_categories_table', 1),
(5, '2018_08_08_173206_create_products_table', 1),
(6, '2018_08_08_173829_create_orders_table', 1),
(7, '2018_08_08_175332_create_order_details_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `music`
--

CREATE TABLE `music` (
  `music_id` int(50) UNSIGNED NOT NULL,
  `album_title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `music_title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `album_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `music_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `music_des` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_ad` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `music`
--

INSERT INTO `music` (`music_id`, `album_title`, `music_title`, `album_image`, `music_url`, `music_des`, `user_id`, `created_at`, `updated_ad`) VALUES
(4, 'TERBITLAH TERANG', 'Wai Rejecteds', 'musics/61d1c52b3477dcb8cd6ed46da03d6dbd.jpg', 'https://open.spotify.com/artist/5rHYaJk7IfXRFOnnZ7aGID', '1. Rewind\r\n2. Tabu\r\n3. Intensi\r\n4. Propaganda\r\n5. Oh My\r\n6. The World Talk', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(10) UNSIGNED NOT NULL,
  `order_user_id` int(11) UNSIGNED NOT NULL,
  `order_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_price` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_zip` int(11) NOT NULL,
  `order_items` int(4) DEFAULT NULL,
  `status` enum('unpaid','paid','delivery','arrived') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'unpaid',
  `order_payment_img` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `order_user_id`, `order_address`, `order_price`, `order_city`, `order_zip`, `order_items`, `status`, `order_payment_img`, `created_at`, `updated_at`) VALUES
(18, 10, 'Jln Adisucipto', '200000', 'Pontianak', 78391, 2, 'arrived', 'payment/c3024e473bf916f5a0697c560830144b.jpg', '2019-07-29 00:35:03', NULL),
(32, 10, 'pontianak', '260000', 'pontianak', 76821, 2, 'arrived', 'payment/2cf6d9dbff981a7b571b1d490eb4752d.jpg', '2019-08-02 22:30:30', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `order_detail_id` int(10) UNSIGNED NOT NULL,
  `order_detail_order_id` int(11) UNSIGNED NOT NULL,
  `order_detail_product_id` int(11) UNSIGNED NOT NULL,
  `order_detail_price` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_detail_qty` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`order_detail_id`, `order_detail_order_id`, `order_detail_product_id`, `order_detail_price`, `order_detail_qty`, `created_at`, `updated_at`) VALUES
(9, 9, 12, '200000', 1, '2018-08-14 15:04:09', NULL),
(10, 10, 7, '135000', 1, '2018-08-14 15:22:47', NULL),
(11, 11, 7, '135000', 1, '2018-08-15 01:10:34', NULL),
(19, 18, 14, '130000', 1, '2019-07-29 00:35:03', NULL),
(20, 18, 7, '70000', 1, '2019-07-29 00:35:03', NULL),
(24, 32, 14, '260000', 2, '2019-08-02 22:30:30', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('admin@email.com', '$2y$10$kRh4RjFWKkvJr.lVE0JIiuj2GAlgm6NtQa4Cq0kOYXBrzdc9/3Roe', '2018-08-13 07:23:01');

-- --------------------------------------------------------

--
-- Table structure for table `photo`
--

CREATE TABLE `photo` (
  `photo_id` int(200) NOT NULL,
  `photo_image` varchar(200) NOT NULL,
  `photo_title` varchar(255) NOT NULL,
  `user_id` int(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `photo`
--

INSERT INTO `photo` (`photo_id`, `photo_image`, `photo_title`, `user_id`) VALUES
(10, 'photos/c9aed6b7e7e4923ee3c97c78d973382f.jpg', 'Theo', 0),
(11, 'photos/53799f22b34450b2af38e4297be8dda1.jpg', 'Edho', 0),
(12, 'photos/df3f8beb9e67aff79de9bf33ee2b7f00.jpg', 'Dhika', 0),
(13, 'photos/31e89f5254e5a7b9306348fda72c5147.jpg', 'John', 0),
(14, 'photos/bbae507b67c92a027b809789535569a6.jpg', 'Singkawang !!!', 0),
(15, 'photos/b7293edc5e65e66051cb0c166c62382b.jpg', 'Tour Khucing', 0),
(17, 'photos/0a68d03c2e868402443cb3f097b7946a.jpg', 'Roadshow Pemuda Inspiratif.', 0),
(18, 'photos/91172cfa0292846a5db707853fc5400d.jpg', 'Wai Rejected X Manjakani', 0);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `product_category_id` int(11) UNSIGNED NOT NULL,
  `product_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_description` text COLLATE utf8mb4_unicode_ci,
  `product_price` double NOT NULL,
  `product_stock` int(11) NOT NULL,
  `product_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_category_id`, `product_title`, `product_size`, `product_description`, `product_price`, `product_stock`, `product_image`, `created_at`, `updated_at`) VALUES
(7, 14, 'CD ALBUM TERBITLAH TERANG', 'All Size', 'CD Album pertama Wai Rejected', 45000, 19, 'product/03a67001b969d2800df84c771bb1462f.jpg', NULL, NULL),
(13, 14, 'GELANG WAI REJECTED', 'All Size', 'Gelang Wai Rejected', 20000, 20, 'product/934c3ed8b0c58e4b5762ea542dcd4f25.jpg', NULL, NULL),
(14, 12, 'Wai Rejected Rewind', 'All Size', 'T-SHIRT WR Rewind Official Merchandise', 130000, 10, 'product/3106a8e4a99cfa51af879a7bab5b3913.jpg', NULL, NULL),
(15, 12, 'Wai Rejected Terbitlah Terang', 'All Size', 'Wai Rejected Terbitlah Terang Official Merchandise', 130000, 9, 'product/caeebf9ba57295f1917867463934c89b.jpg', NULL, NULL),
(16, 9, 'Tote bag Wai Rejected', 'All Size', 'Tote bag Wai Rejected Special Edition 10th (limited stock)', 70000, 10, 'product/b51b1e228dae5c112d1930dd5d437c1f.jpg', NULL, NULL),
(17, 12, 'T-Shirt', 'All Size', 'Tees Wai Rejected Special Edition 10th (limited stock)', 120000, 10, 'product/08cba143b53949bc7d138305f02015fa.jpg', NULL, NULL),
(18, 12, 'T-Shirt KST', 'All Size', 'T-Shirt Wai Rejected KST', 130000, 10, 'product/bc89c7804d55318b26af95bb73e3af2b.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` enum('user','admin') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `user_phone` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_inbox_id` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `user_type`, `user_phone`, `password`, `remember_token`, `created_at`, `updated_at`, `user_inbox_id`) VALUES
(2, 'user', 'user@email.com', 'user', NULL, '$2y$10$LeSeb2VVTlUbO758oWI4Wu.N/D6XstjTOr/PPSSzQdW7yh2cca.rW', 'YXF7rQ6EeETZaY9GmICRujN9AjP9KwZMP9FmlWJVvNNwl8guukar7R5cyskz', '2018-08-08 19:49:01', '2018-08-08 19:49:01', NULL),
(7, 'adminuser', 'adminuser@email.com', 'user', '123123', '$2y$10$alCh3Tys5PuivF9H5UZRXOdrBMxCz1.ikR.qqdZ5GzFczEfTc8PIK', 'Jy10N16tEav2jyqyv1jeAdixpVU3kNwLxkDsKeRF4OZ0eTw14rta3sE7f8fa', '2018-08-13 08:24:01', '2018-08-13 08:24:01', NULL),
(9, 'admin', 'admin@email.com', 'admin', '123123', '$2y$10$LeSeb2VVTlUbO758oWI4Wu.N/D6XstjTOr/PPSSzQdW7yh2cca.rW', 'Cxi05GibFfKH0sLZ8GqihytXVC2dsoIA4Rsjvq8eO0rDKphzl9jkKjb8btA3', '2018-08-13 08:49:11', '2018-08-13 08:49:11', NULL),
(10, 'Wai Rejected', 'WaiRejectedband@gmail.com', 'user', '0899.011.3131', '$2y$10$z5FBAO1vRa/0I7zrEvEiG./1dvZzfzC2T2tr.RdAfUNycnSGDnmx6', 'fDf8sP0VKpMAWlgi6qcDgbMDI3pBzZt5YRF2g8oYPT75xu8UMs2zsSsHMGo2', '2019-07-13 22:14:40', '2019-07-13 22:14:40', NULL),
(11, 'trijayanto?123', 'tri@email.com', 'user', 'tri@email.com', '$2y$10$xKWXgKg3P6yj8fzAdHMCFeTWbSLleZG6Xr7AG5D4.CjVzHHtuwf3S', 'TgUwSAcdJhHFumFAFhFvqokiHsM7J2nijO3DD2WpDfyT5AMqhQTIGu5FriB1', '2019-07-17 01:36:05', '2019-07-17 01:36:05', NULL),
(12, 'i', 'i@gmail.com', 'user', '1234', '$2y$10$rdWgsH5RYXqwrkT.VnPavu8eR5sEQBmct0YUxZtWQd7/9FeB6S1bO', 'im9wG4Ya7SCe8tAUSwo0iwMbEIBhAXDTn4O5wmjNeDooXWFR52BjS9kEDcUE', '2019-07-22 04:15:02', '2019-07-22 04:15:02', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`id_artikel`) USING BTREE,
  ADD KEY `id_artikel` (`id_artikel`) USING BTREE,
  ADD KEY `user_id` (`user_id`) USING BTREE,
  ADD KEY `user_id_2` (`user_id`) USING BTREE;

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`) USING BTREE;

--
-- Indexes for table `inboxes`
--
ALTER TABLE `inboxes`
  ADD PRIMARY KEY (`inbox_id`) USING BTREE;

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `music`
--
ALTER TABLE `music`
  ADD PRIMARY KEY (`music_id`) USING BTREE,
  ADD KEY `user_id` (`user_id`) USING BTREE,
  ADD KEY `user_id_2` (`user_id`) USING BTREE,
  ADD KEY `user_id_3` (`user_id`) USING BTREE,
  ADD KEY `music_id` (`music_id`) USING BTREE,
  ADD KEY `user_id_4` (`user_id`) USING BTREE;

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`) USING BTREE,
  ADD KEY `order_user_id` (`order_user_id`) USING BTREE,
  ADD KEY `order_user_id_2` (`order_user_id`) USING BTREE;

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`order_detail_id`) USING BTREE,
  ADD KEY `order_detail_product_id` (`order_detail_product_id`) USING BTREE,
  ADD KEY `order_detail_order_id` (`order_detail_order_id`) USING BTREE,
  ADD KEY `order_detail_product_id_2` (`order_detail_product_id`) USING BTREE,
  ADD KEY `order_detail_product_id_3` (`order_detail_product_id`) USING BTREE;

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191)) USING BTREE;

--
-- Indexes for table `photo`
--
ALTER TABLE `photo`
  ADD PRIMARY KEY (`photo_id`) USING BTREE,
  ADD KEY `user_id` (`user_id`) USING BTREE;

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`) USING BTREE,
  ADD UNIQUE KEY `product_id_2` (`product_id`) USING BTREE,
  ADD KEY `product_category_id` (`product_category_id`) USING BTREE,
  ADD KEY `product_category_id_2` (`product_category_id`) USING BTREE,
  ADD KEY `product_category_id_3` (`product_category_id`) USING BTREE,
  ADD KEY `product_id` (`product_id`) USING BTREE,
  ADD KEY `product_id_3` (`product_id`) USING BTREE,
  ADD KEY `product_id_4` (`product_id`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `users_email_unique` (`email`) USING BTREE,
  ADD KEY `id` (`id`) USING BTREE,
  ADD KEY `id_2` (`id`) USING BTREE,
  ADD KEY `id_3` (`id`) USING BTREE,
  ADD KEY `user_inbox_id` (`user_inbox_id`) USING BTREE,
  ADD KEY `id_4` (`id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `artikel`
--
ALTER TABLE `artikel`
  MODIFY `id_artikel` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `inboxes`
--
ALTER TABLE `inboxes`
  MODIFY `inbox_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `music`
--
ALTER TABLE `music`
  MODIFY `music_id` int(50) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `order_detail_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `photo`
--
ALTER TABLE `photo`
  MODIFY `photo_id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
