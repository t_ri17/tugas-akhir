<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function upload(Request $request,$path,$field){
        if ($request->hasFile('file')) {
        $file = $request->file('file');
        $filename = md5(Carbon::now()->toAtomString()) . '.' . strtolower($file->getClientOriginalExtension());
        $file->move($path, $filename);
        return $result =$request->request->add([
            $field => $path,$filename
        ]);

    }
    }
}
