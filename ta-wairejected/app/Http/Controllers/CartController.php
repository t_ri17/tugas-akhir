<?php

namespace App\Http\Controllers;

use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Cart::content();
        return view('frontend.page.cart');
    }

    public function update(Request $request,$id)
    {
        Cart::update($id,$request->qty);
        return redirect()->route('cart.index');
    }

    public function delete($id)
    {
        Cart::remove($id);
        return redirect()->route('cart.index');
    }

//    public function store(Request $request)
//    {
//        $data = DB::table('products')->where('product_id',$request->id)->first();
//        Cart::add($request->id,$data->product_title,1,$data->product_price,['img'=>$data->product_image]);
//        return redirect()->back();
//    }
    public function store(Request $request, $id)
    {
        $data = DB::table('products')->where('product_id',$id)->first();

        if ($request->has('qty')) {
            $qty = $request->qty;
        }else{
            $qty = 1;
        }
        foreach (Cart::content() as $item){
            if ($item->id===$id){
                return redirect()->back()->with('error', 'Barang Sudah Ada Dikeranjang');
            }
        }
        Cart::add($id,$data->product_title,$qty,$data->product_price,['img'=>$data->product_image]);
        return redirect()->back()->with('success', 'Barang Telah Masuk Dalam Keranjang');
    }

}
