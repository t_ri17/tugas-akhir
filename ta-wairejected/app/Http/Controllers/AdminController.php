<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\DataTables;

class AdminController extends Controller
{
    public function dashboard()
    {
        return view('admin.page.dashboard');
    }

    public function userIndex()
    {
        return view('admin.page.user.index');
    }

    public function musicIndex()
    {
        return view('admin.page.music.index');
    }

    public function musicForm()
    {
        return view('admin.page.music.form');
    }
    public function musicEdit(Request $request, $id)
    {
        $data = DB::table('music')->where('music_id',$id)->first();
        return view('admin.page.music.form',compact('data'));
    }

    public function musicPost(Request $request)
    {
        $data = DB::table('music');
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $filename = md5(Carbon::now()->toAtomString()) . '.' . strtolower($file->getClientOriginalExtension());
            $file->move('musics/' , $filename);
            $request->request->add([
                'album_image' => 'musics/'.$filename
            ]);
        }
        $data->insert($request->except(['_token','file']));
        return redirect()->route('admin.music.index');
    }

    public function musicGet(DataTables $dtb)
    {
        $data = DB::table('music')
            ->select('*');
        // dd($data->get());
        return $dtb
            ->collection($data->get())
            ->addColumn('action' , function ($row) {
                $id = $row->music_id;
                return '
                <div class="text-center">
                <a class="btn btn-primary" href="' . route('admin.music.edit' , $id) . '"><i class="fa fa-pencil"></i></a>
                <form id="delete-form' . $id . '" action="' . route('admin.music.delete' , ['id' => $id]) . '" method="POST" style="display: inline;" role="form">
                    ' . csrf_field() . '
                    ' . method_field('DELETE') . '
                    <button onClick="isdelete(' . $id . ')" type="button" class="btn btn-danger"><i class="fa fa-bitbucket"></i></button>
                </form> 
                
                </div>
                ';
            })->addColumn('url' , function ($row) {
                return '
                <img src="' . url($row->album_image) . '" height="100px">
            ';
            })->rawColumns(['action', 'url'])
            ->make(true);
    }

    public function musicUpdate(Request $request,$id)
    {
        $data = DB::table('music')->where('music_id',$id);

        if( !empty($request->file('file')) ) {
            if ($request->hasFile('file')) {
                $file = $request->file('file');
                $filename = md5(Carbon::now()->toAtomString()) . '.' . strtolower($file->getClientOriginalExtension());
                $file->move('musics/', $filename);
                $request->request->add([
                    'album_image' => 'musics/' . $filename
                ]);
            }
            $input = [
                'album_title' => $request->album_title,
                'music_title' => $request->music_title,
                'album_image' => $request->album_image,
                'music_url' => $request->music_url,
                'music_des' => $request->music_des,
            ];
        }else{
            $input = [
                'album_title' => $request->album_title,
                'music_title' => $request->music_title,
                'music_url' => $request->music_url,
                'music_des' => $request->music_des,
            ];
        }


        $data->update($input);
        return redirect()->route('admin.music.index');
    }


    public function musicDelete(Request $request,$id)
    {
        $data = DB::table('music')
            ->where('music_id','=',$id);
        if (!empty($data->first()->photo_image)){
            $old = $data->first()->photo_image;
            if(is_file($old)){
                @unlink($old);
            }
        }
        $data->delete();
        return redirect()->route('admin.music.index');
    }
    public function photoIndex()
    {
        return view('admin.page.photo.index');
    }
    public function photoForm()
    {
        return view('admin.page.photo.form');
    }
    public function photoEdit(Request $request, $id)
    {
        $data = DB::table('photo')->where('photo_id',$id)->first();
        return view('admin.page.photo.form',compact('data'));
    }
    public function photoPost(Request $request)
    {
        $data = DB::table('photo');
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $filename = md5(Carbon::now()->toAtomString()) . '.' . strtolower($file->getClientOriginalExtension());
            $file->move('photos/' , $filename);
            $request->request->add([
                'photo_image' => 'photos/'.$filename
            ]);
        }
        $data->insert($request->except(['_token','file']));
        return redirect()->route('admin.photo.index');
    }

    public function photoGet(DataTables $dtb)
    {
        $data = DB::table('photo')
            ->select('*');
        // dd($data->get());
        return $dtb
            ->collection($data->get())
            ->addColumn('action' , function ($row) {
                $id = $row->photo_id;
                return '
                <div class="text-center">
                <a class="btn btn-primary" href="' . route('admin.photo.edit' , $id) . '"><i class="fa fa-pencil"></i></a>
                <form id="delete-form' . $id . '" action="' . route('admin.photo.delete' , ['id' => $id]) . '" method="POST" style="display: inline;" role="form">
                    ' . csrf_field() . '
                    ' . method_field('DELETE') . '
                    <button onClick="isdelete(' . $id . ')" type="button" class="btn btn-danger"><i class="fa fa-bitbucket"></i></button>
                </form> 
                
                </div>
                ';
            })->addColumn('url' , function ($row) {
                return '
                <img src="' . url($row->photo_image) . '" height="100px">
            ';
            })->rawColumns(['action', 'url'])
            ->make(true);
    }
    public function photoUpdate(Request $request,$id)
    {
        $data = DB::table('photo')->where('photo_id',$id);

        if( !empty($request->file('file')) ) {
            if ($request->hasFile('file')) {
                $file = $request->file('file');
                $filename = md5(Carbon::now()->toAtomString()) . '.' . strtolower($file->getClientOriginalExtension());
                $file->move('photos/', $filename);
                $request->request->add([
                    'photo_image' => 'photos/' . $filename
                ]);
            }
            $input = [
                'photo_title' => $request->photo_title,
                'photo_image' => $request->photo_image,
            ];
        }else{
            $input = [
                'photo_title' => $request->photo_title,

            ];
        }


        $data->update($input);
        return redirect()->route('admin.photo.index');
    }


    public function photoDelete(Request $request,$id)
    {
        $data = DB::table('photo')
            ->where('photo_id','=',$id);
        if (!empty($data->first()->photo_image)){
            $old = $data->first()->photo_image;
            if(is_file($old)){
                @unlink($old);
            }
        }
        $data->delete();
        return redirect()->route('admin.photo.index');
    }


    public function artikelIndex()
    {
        return view('admin.page.artikel.index');
    }
    public function artikelForm()
    {
        return view('admin.page.artikel.form');
    }
    public function artikelEdit(Request $request, $id)
    {
        $data = DB::table('artikel')->where('id_artikel',$id)->first();
        return view('admin.page.artikel.form',compact('data'));
    }


    public function artikelPost(Request $request)
    {
        $data = DB::table('artikel');
        if ($request->hasFile('file')) {
           $file = $request->file('file');
           $filename = md5(Carbon::now()->toAtomString()) . '.' . strtolower($file->getClientOriginalExtension());
           $file->move('artikels/' , $filename);
            $request->request->add([
                'artikel_image' => 'artikels/'.$filename
            ]);
        }
        $data->insert($request->except(['_token','file']));
        return redirect()->route('admin.artikel.index');
    }

    public function artikelGet(DataTables $dtb)
    {
        $data = DB::table('artikel')
            ->select('*');
            // dd($data->get());
        return $dtb
            ->collection($data->get())
            ->addColumn('action' , function ($row) {
                $id = $row->id_artikel;
                return '
                <div class="text-center">
                <a class="btn btn-primary" href="' . route('admin.artikel.edit' , $id) . '"><i class="fa fa-pencil"></i></a>
                <form id="delete-form' . $id . '" action="' . route('admin.artikel.delete' , ['id' => $id]) . '" method="POST" style="display: inline;" role="form">
                    ' . csrf_field() . '
                    ' . method_field('DELETE') . '
                    <button onClick="isdelete(' . $id . ')" type="button" class="btn btn-danger"><i class="fa fa-bitbucket"></i></button>
                </form> 
                
                </div>
                ';
            })->addColumn('url' , function ($row) {
                return '
                <img src="' . url($row->artikel_image) . '" height="100px">
            ';
            })->rawColumns(['action', 'url'])
            ->make(true);
    }
    public function artikelUpdate(Request $request,$id)
    {
        $data = DB::table('artikel')->where('id_artikel',$id);

        if( !empty($request->file('file')) ) {
            if ($request->hasFile('file')) {
                $file = $request->file('file');
                $filename = md5(Carbon::now()->toAtomString()) . '.' . strtolower($file->getClientOriginalExtension());
                $file->move('artikels/', $filename);
                $request->request->add([
                    'artikel_image' => 'artikels/' . $filename
                ]);
            }
            $input = [
                'artikel_title' => $request->artikel_title,
                'artikel_image' => $request->artikel_image,
                'artikel_url' => $request->artikel_url,
                'artikel_des' => $request->artikel_des,
            ];
        }else{
            $input = [
                'artikel_title' => $request->artikel_title,
                'artikel_url' => $request->artikel_url,
                'artikel_des' => $request->artikel_des,
            ];
        }


        $data->update($input);
        return redirect()->route('admin.artikel.index');
    }



    public function artikelDelete(Request $request,$id)
    {
        $data = DB::table('artikel')
            ->where('id_artikel','=',$id);
        if (!empty($data->first()->artikel_image)){
            $old = $data->first()->artikel_image;
            if(is_file($old)){
                @unlink($old);
            }
        }
        $data->delete();
        return redirect()->route('admin.artikel.index');
    }


    public function userGet(DataTables $dtb)
    {
        $data = DB::table('users')
            ->select('*');
        return $dtb
            ->collection($data->get())
            ->addColumn('action' , function ($row) {
                $id = $row->id;
                return '
                <div class="text-center">
                <a class="btn btn-primary" href="' . route('admin.user.edit' , $id) . '"><i class="fa fa-pencil"></i></a>
                <form id="delete-form' . $id . '" action="' . route('admin.user.delete' , ['id' => $id]) . '" method="POST" style="display: inline;" role="form">
                    ' . csrf_field() . '
                    ' . method_field('DELETE') . '
                    <button onClick="isdelete(' . $id . ')" type="button" class="btn btn-danger"><i class="fa fa-bitbucket"></i></button>
                </form> 
                
                </div>
                ';
            })
            ->rawColumns(
                [
                    'action' ,
                ])
            ->make(true);
    }

    public function userForm()
    {
        return view('admin.page.user.form');
    }

    public function userEdit(Request $request, $id)
    {
        $data = DB::table('users')->where('id',$id)->first();
        return view('admin.page.user.form',compact('data'));
    }

    public function userPost(Request $request)
    {
        $data = new \stdClass();
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'password-confirm' => 'same:password|min:6',
            'user_phone' => 'required|string|max:15',
        ]);
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'user_phone' => $request->user_phone,
            'user_type' => $request->user_type,
            'password' => Hash::make($request->password),
        ]);

        return view('admin.page.user.index');
    }

    public function userUpdate(Request $request,$id)
    {
        $data = User::findOrFail($id);



        if (!empty($request->password)) {
            $request->validate([
            'name' => 'required|string|max:255',
            'password' => 'min:6',
            'password-confirm' => 'same:password|min:6',
            'user_phone' => 'required|string|max:15',
               ]);
            $data->password = bcrypt($request->password);

        }else {
            $request->validate([
            'name' => 'required|string|max:255',
            'user_phone' => 'required|string|max:15',
               ]);
        }
        $data->update([
            'name' => $request->name,
            'email' => $request->email,
            'user_type' => $request->user_type,
            'user_phone' => $request->user_phone,
            ]);
         return redirect()->route('admin.user.index');
    }

    public function userDelete(Request $request,$id)
    {
        $data = new \stdClass();
        DB::table('users')->where('id','=',$id)->delete();
        $data->l = ['success'=>'Success Delete User'];
        return redirect()->route('admin.user.index','data');
    }

    public function categoryIndex()
    {
        return view('admin.page.category.index');
    }

    public function categoryGet(DataTables $dtb)
    {
        $data = DB::table('categories')
            ->select('*');
        return $dtb
            ->collection($data->get())
            ->addColumn('action' , function ($row) {
                $id = $row->category_id;
                return '
                <div class="text-center">
                <a class="btn btn-primary" href="' . route('admin.category.edit' , $id) . '"><i class="fa fa-pencil"></i></a>
                <form id="delete-form' . $id . '" action="' . route('admin.category.delete' , ['id' => $id]) . '" method="POST" style="display: inline;" role="form">
                    ' . csrf_field() . '
                    ' . method_field('DELETE') . '
                    <button onClick="isdelete(' . $id . ')" type="button" class="btn btn-danger"><i class="fa fa-bitbucket"></i></button>
                </form> 
                
                </div>
                ';
            })
            ->rawColumns(
                [
                    'action' ,
                ])
            ->make(true);
    }

    public function categoryForm()
    {
        return view('admin.page.category.form');
    }

    public function categoryEdit(Request $request, $id)
    {
        $data = DB::table('categories')->where('category_id',$id)->first();

        return view('admin.page.category.form',compact('data'));
    }

    public function categoryPost(Request $request)
    {
        $data = DB::table('categories');
        $input = [
            'category_name' => $request->category_name,
        ];
        $data->insert($input);
        return view('admin.page.category.index');
    }

    public function categoryUpdate(Request $request,$id)
{
    $data = DB::table('categories')->where('category_id',$id);
    $input = [
        'category_name' => $request->category_name,
    ];
    $data->update($input);
    return redirect()->route('admin.category.index');
}

    public function categoryDelete(Request $request,$id)
    {
        $data = DB::table('categories')
            ->where('category_id','=',$id);
        $data->delete();
        return redirect()->route('admin.category.index');
    }

    public function productIndex()
    {
        return view('admin.page.product.index');
    }

    public function productGet(DataTables $dtb)
    {
        $data = DB::table('products')
            ->select('products.*','categories.*')
            ->leftJoin('categories','category_id','=','product_category_id');
        return $dtb
            ->collection($data->get())
            ->addColumn('action' , function ($row) {
                $id = $row->product_id;
                return '
                <div class="text-center">
                <a class="btn btn-primary" href="' . route('admin.product.edit' , $id) . '"><i class="fa fa-pencil"></i></a>
                <form id="delete-form' . $id . '" action="' . route('admin.product.delete' , ['id' => $id]) . '" method="POST" style="display: inline;" role="form">
                    ' . csrf_field() . '
                    ' . method_field('DELETE') . '
                    <button onClick="isdelete(' . $id . ')" type="button" class="btn btn-danger"><i class="fa fa-bitbucket"></i></button>
                </form> 
                
                </div>
                ';
            })->addColumn('url' , function ($row) {
                return '
                <img src="' . url($row->product_image) . '" height="100px">
            ';
            })->rawColumns(['action', 'url'])
            ->make(true);
    }

    public function productForm()
    {
        $data = new \stdClass();
        $data->category = DB::select('select * from categories');
        return view('admin.page.product.form',compact('data'));
    }

    public function productEdit(Request $request, $id)
    {
        $data = new \stdClass();
        $data->product = DB::table('products')->where('product_id',$id)->first();
        $data->category = DB::select('select * from categories');
        return view('admin.page.product.form',compact('data'));
    }

    public function productPost(Request $request)
    {
        $data = DB::table('products');
        if ($request->hasFile('file')) {
           $file = $request->file('file');
           $filename = md5(Carbon::now()->toAtomString()) . '.' . strtolower($file->getClientOriginalExtension());
           $file->move('product/' , $filename);
            $request->request->add([
                'product_image' => 'product/'.$filename
            ]);
        }
        $data->insert($request->except(['_token','file']));
        return redirect()->route('admin.product.index');
    }

    public function productUpdate(Request $request,$id)
    {
        $data = DB::table('products')->where('product_id',$id);
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            if (!empty($data->first()->product_image)){
                $old = $data->first()->product_image;
                if(is_file($old)){
                    @unlink($old);
                }
            }
            $filename = md5(Carbon::now()->toAtomString()) . '.' . strtolower($file->getClientOriginalExtension());
            $file->move('product/' , $filename);
            $request->request->add([
                'product_image' => 'product/'.$filename
            ]);
        }
        $data->update($request->except(['_token','file']));
        return redirect()->route('admin.product.index');
    }

    public function productDelete(Request $request,$id)
    {
        $data = DB::table('products')
            ->where('product_id','=',$id);
        if (!empty($data->first()->product_image)){
            $old = $data->first()->product_image;
            if(is_file($old)){
                @unlink($old);
            }
        }
        $data->delete();
        return redirect()->route('admin.product.index');
    }

    public function inboxIndex(Request $request,$p)
    {
        $data = new \stdClass();

        if ($p === 'to_id' ){
            $data->title = 'Sended';
        }
        else {
            $data->title = 'Inbox';
        }

        return view('admin.page.inbox.index',compact('data','p'));
    }

    public function inboxForm()
    {
        $data = new \stdClass();
        $data->user = DB::select('select * from users');
        return view('admin.page.inbox.form',compact('data'));
    }

    public function inboxGet(DataTables $dtb,$p)
    {
        $data = DB::table('inboxes')
            ->select('inboxes.*','users.*')
            ->leftJoin('users','id','=','inbox_'.$p)
            ->where('inbox_'.$p,'!=',Auth::user()->id);
        return $dtb
            ->collection($data->get())
            ->addColumn('action' , function ($row) {
                $id = $row->inbox_id;
                return '
                <div class="text-center">
                <form id="delete-form' . $id . '" action="' . route('admin.inbox.delete' , ['id' => $id]) . '" method="POST" style="display: inline;" role="form">
                    ' . csrf_field() . '
                    ' . method_field('DELETE') . '
                    <button onClick="isdelete(' . $id . ')" type="button" class="btn btn-danger"><i class="fa fa-bitbucket"></i></button>
                </form> 
                
                </div>
                ';
            })->rawColumns(['action'])
            ->make(true);
    }

    public function inboxPost(Request $request)
    {
        $data = DB::table('inboxes');
        $request->request->add([
            'inbox_from_id' => Auth::user()->id
        ]);
        $data->insert($request->except(['_token']));
        return redirect()->route('admin.inbox.index',['p' => 'to_id']);
    }

    public function inboxDelete(Request $request,$id)
    {
        DB::table('inboxes')->where('inbox_id','=',$id)->delete();
        return redirect()->route('admin.inbox.index',['p' => 'to_id']);
    }

    public function orderIndex()
    {
        return view('admin.page.order.index');
    }

    public function orderGet(DataTables $dtb)
    {
        $data = DB::table('orders')
            ->select('orders.*','users.name')
            ->leftJoin('users','id','=','order_user_id');
        return $dtb
            ->collection($data->get())
            ->addColumn('action' , function ($row) {
                $id = $row->order_id;
                return '
                <div class="text-center">
                <a class="btn btn-primary" href="' . route('admin.order.edit' , $id) . '"><i class="fa fa-pencil"></i></a>
                <form id="delete-form' . $id . '" action="' . route('admin.order.delete' , ['id' => $id]) . '" method="POST" style="display: inline;" role="form">
                    ' . csrf_field() . '
                    ' . method_field('DELETE') . '
                    <button onClick="isdelete(' . $id . ')" type="button" class="btn btn-danger"><i class="fa fa-bitbucket"></i></button>
                </form> 
                
                </div>
                ';
            })
            ->addColumn('img' , function ($row) {

                if ($row->order_payment_img == null) {
                    return "Nothing Image";
                } else {
                    return '
                    <img src="' . url($row->order_payment_img) . '" height="150px">
                    ';
                }
            })
            ->rawColumns(
                [
                    'action' ,
                    'img'
                ])
            ->make(true);
    }

    public function orderEdit($id)
    {
        $data = new \stdClass();
        $data->order = DB::table('orders')
            ->select('orders.*','users.name')
            ->leftJoin('users','id','=','order_user_id')
            ->where('order_id',$id)
            ->first();
        $data->od = DB::table('order_details')
            ->leftJoin('products','product_id','=','order_detail_product_id')
            ->where('order_detail_order_id',$id)
            ->get();
        $data->his = DB::table('order_details')
            ->select(DB::raw('created_at'),DB::raw('SUM(order_detail_price) as s'),DB::raw('SUM(order_detail_qty) as i'),
                DB::raw('COUNT(order_detail_order_id) as cc'))
            ->where('order_detail_order_id',$id)
            ->groupBy('created_at')
            ->first();
        return view('admin.page.order.form',compact('data'));
    }

    public function orderUpdate(Request $request,$id)
    {

        $data = DB::table('orders')->where('order_id',$id);

        if ($request->status === 'paid') {
            $produkid = DB::table('order_details')->where('order_detail_order_id',$id)->get();

        foreach ($produkid as $stok){
            $products = DB::table('products')->where('product_id',$stok->order_detail_product_id)->get();
            $jumlahstok = [
              'product_stock' => (($products->first()->product_stock)-($stok->order_detail_qty)),
            ];
            DB::table('products')->where('product_id',$stok->order_detail_product_id)->update($jumlahstok);
        }

        }



        $input = [
            'status' => $request->status,
        ];
        $data->update($input);
        return redirect()->back();
    }

    public function orderDelete($id){
        DB::table('order_details')->where('order_detail_order_id',$id)->delete();
        DB::table('orders')->where('order_id',$id)->delete();
        return redirect()->back();
    }

 public function reportIndex(Request $request){
       $data = new \stdClass();
            $data->count=0;
            $data->report = DB::table('order_details')
                    ->where('status', '=', 'paid')
                    ->leftJoin('orders', 'order_id', '=', 'order_details.order_detail_order_id')
                    ->leftJoin('products', 'product_id', '=', 'order_details.order_detail_product_id')
                    ->select('*')
                    ->selectRaw('order_details.created_at as oc')->get();
    return view ('admin.page.report.report',(compact('data')));

 }
public function orderDetailDelete(Request $request,$id)
    {
        $data = DB::table('order_details')
            ->where('order_detail_id','=',$id);
        $data->delete();
        return redirect()->back();
    }


}
