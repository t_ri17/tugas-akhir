<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class HomeController extends Controller
{
    public function dashboard()
    {
        $data = new \stdClass();
        $data->product = DB::table('products')
                        ->select('products.*','categories.*')
                        ->leftJoin('categories','category_id','=','product_category_id')->get();


        return view('frontend.page.home',compact('data'));
    }

    public function category(Request $request)
    {
        $data = new \stdClass();
        $data->category= DB::table('categories')
            ->leftJoin('products','category_id','=','product_category_id')
            ->groupBy('category_id','category_name','product_category_id')
            ->select('category_name','category_id', DB::raw('COUNT(product_category_id) as cc'))
            ->where('product_stock','>',0)
            ->get();


            if ($request->has('cat')) {
            $data->product = DB::table('products')
                 ->select('products.*','categories.*')
                 ->leftJoin('categories','category_id','=','product_category_id')
                 ->where('product_category_id','=',$request->cat)->paginate(9);
            }else{
            $data->product = DB::table('products')
                 ->select('products.*','categories.*')
                 ->leftJoin('categories','category_id','=','product_category_id')
                 ->paginate(9);
            }

        return view('frontend.page.category',compact('data'));
    }

    public function product(Request $request)
    {
        $data = new \stdClass();
        $data->category= DB::table('categories')
            ->leftJoin('products','category_id','=','product_category_id')
            ->groupBy('category_id','category_name','product_category_id')
            ->select('category_name','category_id', DB::raw('COUNT(product_category_id) as cc'))
            ->get();
        if ($request->has('q')) {
            $data->product = DB::table('products')
                ->select('products.*','categories.*')
                ->leftJoin('categories','category_id','=','product_category_id')
                ->where("product_title" , 'like' , "%$request->q%")
                ->orWhere("product_description" , 'like' , "%$request->q%")
                ->paginate(12);
        }else{
            $data->product = DB::table('products')
                ->select('products.*','categories.*')
                ->leftJoin('categories','category_id','=','product_category_id')
                ->paginate(12);
        }

        return view('frontend.page.product',compact('data'));
    }

    public function productDetail($id)
    {
        $data = new \stdClass();
        $data->category= DB::table('categories')
            ->leftJoin('products','category_id','=','product_category_id')
            ->groupBy('category_id','category_name','product_category_id')
            ->select('category_name','category_id', DB::raw('COUNT(product_category_id) as cc'))
            ->get();
        $data->product = DB::table('products')
            ->select('products.*','categories.*')
            ->leftJoin('categories','category_id','=','product_category_id')
            ->where('product_id',$id)
            ->first();
        return view('frontend.page.detail',compact('data'));
    }
    public function musicIndex()
    {
            $data = new \stdClass();
            $data->music= DB::table('music')
                ->get();
            return view('frontend.page.music',compact('data'));
    }
    public function photoIndex()
    {
        $data = new \stdClass();
        $data->photo= DB::table('photo')
            ->get();
        return view('frontend.page.photo',compact('data'));
    }

    public function inboxIndex()
    {
        $data = new \stdClass();
        $data->user = DB::select('select * from users where user_type = "admin"');
        return view('frontend.page.inbox',compact('data'));
    }

    public function inboxGet(DataTables $dtb,$p)
    {
        $data = DB::table('inboxes')
            ->join('users as t','t.id','=','inbox_to_id')
            ->join('users as f','f.id','=','inbox_from_id')
            ->selectRaw('inboxes.*,t.name as toname, f.name as fname')
            ->where('inbox_'.$p,'=',Auth::user()->id);

        return $dtb
            ->collection($data->get())
            ->addColumn('action' , function ($row) {
                $id = $row->inbox_id;
                return '
                <div class="text-center">
                <form id="delete-form' . $id . '" action="' . route('inbox.delete' , ['id' => $id]) . '" method="POST" style="display: inline;" role="form">
                    ' . csrf_field() . '
                    ' . method_field('DELETE') . '
                    <button onClick="isdelete(' . $id . ')" type="button" class="btn btn-danger"><i class="fa fa-bitbucket"></i></button>
                </form> 
                
                </div>
                ';
            })->rawColumns(['action'])
            ->make(true);
    }

    public function inboxPost(Request $request)
    {
        $request->request->add([
            'inbox_from_id' => Auth::user()->id
        ]);
        DB::table('inboxes')->insert($request->except(['_token']));
        return redirect()->route('inbox.index');
    }

    public function inboxDelete(Request $request,$id)
    {
        DB::table('inboxes')->where('inbox_id','=',$id)->delete();
        return redirect()->route('inbox.index');
    }

    public function checkoutIndex()
    {
        return view('frontend.page.checkout');
    }

    public function checkoutstore(Request $request)
    {

        $order = DB::table('orders');

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $filename = md5(Carbon::now()->toAtomString()) . '.' . strtolower($file->getClientOriginalExtension());
            $file->move('payment/' , $filename);
            $request->request->add([
                'order_payment_img' => 'payment/'.$filename
            ]);
        }
        $request->request->add(['order_items'=>Cart::count(),'created_at'=>Carbon::now()->toDateTimeString(),'order_user_id'=>Auth::user()->id,'order_price'=>Cart::subtotal(0, ',','')]);

        $order_id = $order->insertGetId($request->except('_token','file','product_id'));

        foreach (Cart::content() as $item){
            DB::table('order_details')->insert([
                'order_detail_order_id' => $order_id,
                'order_detail_product_id'=> $item->id,
                'order_detail_price'=> $item->subtotal,
                'order_detail_qty'=> $item->qty,
                'created_at'=>Carbon::now()->toDateTimeString(),
            ]);
        }
        Cart::destroy();
        return redirect()->route('history.index');
    }

    public function historyIndex()
    {
        $data = new \stdClass();
        $data->his = DB::table('orders')
                ->select(DB::raw('SUM(order_price) as s'),DB::raw('SUM(order_items) as i'),DB::raw('COUNT(order_user_id) as cc'))
                ->where('order_user_id',Auth::user()->id)->first();


        return view('frontend.page.history',compact('data'));
    }

    public function historyGet(DataTables $dtb)
    {
        $data = DB::table('orders')
            ->where('order_user_id',Auth::user()->id);
        return $dtb
            ->collection($data->get())
            ->addColumn('action' , function ($row) {
                $id = $row->order_id;
                return '
                <div class="text-center">
                <a href="'.route('history.detail',['id'=>$id]).'" type="button" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                <form id="delete-form' . $id . '" action="' . route('history.delete' , ['id' => $id]) . '" method="POST" style="display: inline;" role="form">
                    ' . csrf_field() . '
                    ' . method_field('DELETE') . '
                    <button onClick="isdelete(' . $id . ')" type="button" class="btn btn-danger"><i class="fa fa-bitbucket"></i></button>
                </form> 
                
                </div>
                ';
            })->rawColumns(['action'])
            ->make(true);
    }

    public function historyDetail($id)
    {
        $data  = new \stdClass();
        $data->od = DB::table('order_details')
            ->leftJoin('products','product_id','=','order_detail_product_id')
            ->where('order_detail_order_id',$id)
            ->get();
        $data->his = DB::table('order_details')
            ->select(DB::raw('created_at'),DB::raw('SUM(order_detail_price) as s'),DB::raw('SUM(order_detail_qty) as i'),DB::raw('COUNT(order_detail_order_id) as cc'))
            ->where('order_detail_order_id',$id)
            ->groupBy('created_at')
            ->first();
        return view('frontend.page.historydetail',compact('data'));

    }

    public function historyDelete($id)
    {
        DB::table('order_details')->where('order_detail_order_id',$id)->delete();
        DB::table('orders')->where('order_id',$id)->delete();
        return redirect()->back();
    }
    public function about()
    {
        return view('frontend.page.about');
    }
    public function music()
    {
        $data = new \stdClass();
        $data->music= DB::table('music')
            ->select('*')
            ->get();

        return view('frontend.page.music', compact('data'));
    }
    public function photo()
    {
        $data = new \stdClass();
        $data->photo= DB::table('photo')
            ->select('*')
            ->get();

        return view('frontend.page.photo', compact('data'));
    }
    public function vidio()
    {
        return view('frontend.page.vidio');
    }
    public function artikel()
    {
        $data = new \stdClass();
        $data->artikel= DB::table('artikel')
            ->select('*')
            ->get();

        return view('frontend.page.artikel', compact('data'));
    }
    public function howtobuy()
    {
        return view('frontend.page.howtobuy');
    }
    public function faq()
    {
        return view('frontend.page.faq');
    }
    public function contact()
    {
        return view('frontend.page.contact');
    }
}
