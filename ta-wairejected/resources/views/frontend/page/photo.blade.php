@extends('frontend.template')
@section('title', 'photo')
@section('content')
    <section>
        <div class="second-page-container">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="header-for-light">
                            <h1 class="wow fadeInRight animated" data-wow-duration="1s">Photo <span> Wai Rejected</span></h1>
                        </div>

                        <div class="gallery-area section bg-white pt-120 pb-120">
                        <div class="container">
                        <div class="row">
                            @foreach (@$data->photo as $item)
                                <div data-toggle="modal" data-target="#myModal" class="gallery-item col-lg-3 col-md-6 col-sm-8 col-sm-10 col-xs-12" style="margin: 0;padding: 0;cursor: pointer;">
                                    <img src="{{asset($item->photo_image)}}">
                                </div>
                            @endforeach
                        </div>

                                    </div>
                                </div>
                            </div>
                </div>
            </div>
        </div>


    </section>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="background-color: #555;">
                <div class="modal-header" style="border-bottom: 0px solid #e5e5e5;padding-bottom: 5px;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="opacity: 1;color: #ff0000"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body" style="padding-top: 5px;"></div>
            </div>
        </div>
    </div>


@endsection


