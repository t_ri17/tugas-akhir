@extends('frontend.template')
@section('title', 'music')
@section('content')
    <section>
        <div class="second-page-container">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="header-for-light">
                            <h1 class="wow fadeInRight animated" data-wow-duration="1s">Album <span> Wai Rejected</span></h1>
                        </div>

                        <div class="grid-container-music">
                            <div class="grid-container-music">
                                @foreach (@$data->music as $item)
                                    <div class="grid-music"><img src="{{asset($item->album_image)}}">
                                        <div style=" color: #7c1212 " >{{$item->album_title}}</div>
                                        <ul style="text-align: left">
                                            <li> Rewind
                                            </li>
                                            <li> Tabu
                                            </li>
                                            <li> Intensi
                                            </li>
                                            <li> The World Talk
                                            </li>
                                            <li> Propaganda
                                            </li>
                                            <li> oh My
                                            </li>
                                            <li> Terbitlah Terang
                                            </li>
                                            <li> Luna
                                            </li>
                                            <li> Why
                                            </li>

                                        </ul>


                                        {{--<div style="text-align: center"> {{$item->music_des}} </div>--}}
                                        <div><a href="{{$item->music_url}}">Play Now</a> </div>
                                    <br><br></div>
                                    </div>
                                @endforeach
                            </div>

    </section>
@endsection


