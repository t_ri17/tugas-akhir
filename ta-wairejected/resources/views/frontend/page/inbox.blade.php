@extends('frontend.template')
@section('title', 'Message')
@section('content')
    <section>
        <div class="second-page-container">
            <div>
                <div class="container">
                    <div class="header-for-light">
                        <h1 class="wow fadeInRight animated" data-wow-duration="1s"><span>Send</span> @yield('title') </h1>
                    </div>
                    <div class="row">
                        <article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="block-form box-border wow fadeInLeft animated" data-wow-duration="1s">
                                <h3><i class="fa fa-envelope-o"></i>Send Message</h3>
                                <form method="post" action="{{ route('inbox.post') }}">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="inputSubject" class="control-label">Send to:<span class="text-error">*</span></label>
                                                <div>
                                                    <select id="inputSubject" name="inbox_to_id" class="form-control" required>
                                                        <option value="">Choose Receiver</option>
                                                        @foreach(@$data->user as $item)
                                                            @if($item->id !== Auth::user()->id)
                                                                <option value="{{@$item->id}}" {{old('inbox_to_id') == @$item->id ? "selected" : @$item->id == @$data->product->inbox_to_id ? 'selected' : null}}>{{@$item->name}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('inbox_to_id'))
                                                        <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('inbox_to_id') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="text" class="col-sm-3 control-label">Message:<span class="text-error">*</span></label>
                                                <div>
                                                   <textarea id="text" class="form-control" name="inbox_text" required value=""></textarea>
                                                    @if ($errors->has('inbox_text'))
                                                        <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('inbox_text') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    <hr>
                                    <input type="submit" class="btn-default-1">
                                    </div>
                                </form>
                            </div>
                        </article>
                        <article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="block-form box-border wow fadeInRight animated" data-wow-duration="1s">
                                <h3> <i class="fa fa-envelope"></i>Inbox Message</h3>
                                <hr>
                                <table id="table-layout" class="table table-bordered table-striped" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>From</th>
                                        <th>Message</th>
                                        <th class="" style="text-align: center; width: 20%;"><span class="fa fa-ellipsis-v "></span></th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </article>
                        <article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="block-form box-border wow fadeInRight animated" data-wow-duration="1s">
                                <h3> <i class="fa fa-envelope"></i>Sended Message</h3>
                                <hr>
                                <table id="table-layout1" class="table table-bordered table-striped" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Send To</th>
                                        <th>Message</th>
                                        <th class="" style="text-align: center; width: 20%;"><span class="fa fa-ellipsis-v "></span></th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
<script>
    var table = $('#table-layout').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('inbox.get',['p'=>'to_id']) }}",
        columns: [
            {data: 'inbox_id', name: 'inbox_id'},
            {data: 'fname', name: 'fname'},
            {data: 'inbox_text', name: 'inbox_text'},
            {data: 'action', name: 'action', searchable: false, orderable: false}
        ],
    });

    var table1 = $('#table-layout1').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('inbox.get',['p'=>'from_id']) }}",
        columns: [

            {data: 'inbox_id', name: 'inbox_id'},
            {data: 'toname', name: 'toname'},
            {data: 'inbox_text', name: 'inbox_text'},
            {data: 'action', name: 'action', searchable: false, orderable: false}
        ],
    });

    function isdelete(k) {
        swal({
                title: "Are you sure?",
                text: "Data Akan Terhapus!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#fb483a',
                confirmButtonText: "Yes, delete it!"
            },
            function(){
                $('#delete-form'+k).submit();
            });
    }
</script>
    @endsection
