@extends('frontend.template')
@section('title', 'Categories')
@section('content')
    <section>
        <div class="second-page-container">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">

                        <div class="header-for-light">
                            <h1 class="wow fadeInRight animated" data-wow-duration="1s">Category <span>Product {{ $data->product->isEmpty() ? 'not found !!!' : null }}</span></h1>
                        </div>

                        <div class="row">
                                @foreach($data->product as $item)
                                @if($item->product_stock>0)
                                    <div class="col-xs-12 col-sm-6 col-md-4 text-center mb-25">
                                        <article class="product light">
                                            <figure class="figure-hover-overlay" style="background-image: url({{ url($item->product_image) }}); background-size: cover; background-position: 50% 50%; width: 253px; height: 316px">
                                                <a href="#"  class="figure-href"></a>
                                                {{--<div class="product-new">new</div>--}}
                                                {{--<div class="product-sale">11% <br> off</div>--}}
                                                <a href="{{route('productDetail',['id'=>$item->product_id])}}" class="product-compare"><i class="fa fa-eye"></i></a>
                                                <img src="#" class="img-responsive" alt="">
                                            </figure>
                                            <div class="product-caption">
                                                <div class="block-name">
                                                    <a href="#" class="product-name">{{ $item->product_title }}</a>
                                                    <p class="product-price">Rp {{ $item->product_price }}</p>
                                                </div>
                                                <div class="product-cart">
                                                    <a
                                                        onclick="event.preventDefault();
                                                            document.getElementById('cart-form{{$item->product_id}}').submit();">
                                                        <i class="fa fa-shopping-cart"></i>
                                                    </a>
                                                    <form id="cart-form{{$item->product_id}}" action="{{ route('cart.store',['id'=>$item->product_id]) }}" method="POST" style="display: none;">
                                                        @csrf
                                                        {{--<input type="hidden" name="id" value="{{ $item->product_id }}">--}}
                                                    </form>
                                                </div>
                                            <div class="category-title">
                                                <a href="{{route('category','cat='.$item->category_id)}}" class="review">{{ $item->category_name }}</a>
                                            </div>
                                                <p class="description">{{ $item->product_description }}</p>
                                            </div>
                                        </article>
                                    </div>
                                    @endif
                                @endforeach
                        </div>
                        <div class="block-pagination">
                            <ul class="pagination">
                                {{$data->product->links()}}
                            </ul>
                        </div>
                    </div>
                    @include('frontend.layout.category')
                </div>
            </div>
        </div>

    </section>
@endsection


