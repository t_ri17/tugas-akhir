@extends('frontend.template')
@section('title', 'Product')
@section('content')
    <section>
        <div class="second-page-container">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <div class="header-for-light">
                            <h1 class="wow fadeInRight animated" data-wow-duration="1s">All <span>Product</span></h1>
                        </div>
                        {{--<div class="block-products-modes color-scheme-2">--}}
                        {{--<div class="row">--}}
                        {{--<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">--}}
                        {{--<div class="product-view-mode">--}}
                        {{--<a href="products-grid.html" class="active"><i class="fa fa-th-large"></i></a>--}}
                        {{--<a href="products-list.html"><i class="fa fa-th-list"></i></a>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">--}}
                        {{--<div class="row">--}}
                        {{--<div class="col-md-3 col-md-offset-1">--}}
                        {{--<label class="pull-right">Sort by</label>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-5">--}}
                        {{--<select name="sort-type" class="form-control">--}}
                        {{--<option value="position:asc">--</option>--}}
                        {{--<option value="price:asc"  selected="selected">Price: Lowest first</option>--}}
                        {{--<option value="price:desc">Price: Highest first</option>--}}
                        {{--<option value="name:asc">Product Name: A to Z</option>--}}
                        {{--<option value="name:desc">Product Name: Z to A</option>--}}
                        {{--<option value="quantity:desc">In stock</option>--}}
                        {{--</select>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-3">--}}
                        {{--<select name="products-per-page" class="form-control">--}}
                        {{--<option value="10" selected="selected">10</option>--}}
                        {{--<option value="20">20</option>--}}
                        {{--<option value="30">30</option>--}}
                        {{--<option value="100">100</option>--}}
                        {{--<option value="all">All</option>--}}
                        {{--</select>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        <div class="row">
                            @foreach(@$data->product as $item)
                            @if($item->product_stock>0)
                            <div class="col-xs-12 col-sm-6 col-md-3 text-center mb-25">
                                <article class="product light">
                                    <figure class="figure-hover-overlay" style="background-image: url({{ url($item->product_image) }}); background-size: cover; background-position: 50% 50%; width: 253px; height: 316px">
                                        <a href="#"  class="figure-href"></a>
                                        {{--<div class="product-new">new</div>--}}
                                        {{--<div class="product-sale">11% <br> off</div>--}}
                                        <a href="{{route('productDetail',['id'=>$item->product_id])}}" class="product-compare"><i class="fa fa-eye"></i></a>
                                        <img src="#" class="img-responsive" alt="">
                                    </figure>
                                    <div class="product-caption">
                                        <div class="block-name">
                                            <a href="#" class="product-name">{{ $item->product_title }}</a>
                                            <p class="product-price">Rp {{ $item->product_price }}</p>
                                        </div>
                                        <div class="product-cart">
                                            <a
                                                    onclick="event.preventDefault();
                                                            document.getElementById('cart-form{{$item->product_id}}').submit();">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <form id="cart-form{{$item->product_id}}" action="{{ route('cart.store',['id'=>$item->product_id]) }}" method="POST" style="display: none;">
                                                @csrf{{--
                                                <input type="hidden" name="id" value="{{ $item->product_id }}">--}}
                                            </form>
                                        </div>
                                       <div class="category-title">
                                        <a href="{{route('category','cat='.$item->category_id)}}" class="review">{{ $item->category_name }}</a>
                                    </div>
                                        <p class="description">{{ $item->product_description }}</p>
                                    </div>
                                </article>
                            </div>
                            @endif
                            @endforeach
                        </div>
                        <div class="block-pagination">
                            <ul class="pagination">
                                {{$data->product->links()}}
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </section>
@endsection


