@extends('frontend.template')
@section('title', 'Home')
@section('content')
<section>
    <div class="second-page-container">
        <div class="block">
            <div class="container">
                <div class="header-for-light">
                    <h1 class="wow fadeInRight animated" data-wow-duration="1s"><span>Checkout</span> Details</h1>
                </div>

                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                        <div class="box-border block-form wow fadeInLeft" data-wow-duration="1s">
                            <!-- Tab panes -->
                            @if (Cart::count() > 0)
                            <div class="tab-content">
                                <div class="tab-pane active" id="address">
                                    <br>
                                    <h3>Account & Billing Details</h3>
                                    <hr>
                                    <form role="form" method="post" action="{{ route('checkout.store') }}" enctype="multipart/form-data">
                                        @csrf
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="inputAdress1" class="control-label">Address : <span class="text-error">*</span></label>
                                                    <div>
                                                        <input type="text" name="order_address" class="form-control" id="inputAdress1" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputCity" class="control-label">City: <span class="text-error">*</span></label>
                                                    <div>
                                                        <input type="text" name="order_city" class="form-control" id="inputCity" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputPostCode" class="control-label">Post Code: <span class="text-error">*</span></label>
                                                    <div>
                                                        <input type="text" name="order_zip" class="form-control" id="inputPostCode" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="file" class="control-label">Upload Transaction : <span class="text-error">*</span></label>
                                                    <div>
                                                        <input type="file" name="file" class="form-control" id="file" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div>
                                                        <button type="submit" class="btn btn-danger">Submit</button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </form>
                                    <hr>
                                </div>
                            </div>
                            @else
                                <h1>No Item for Check Out</h1>
                                <br>
                                <a href="{{ route('product') }}" class="btn btn-danger">Continue Shoping</a>
                            @endif
                        </div>
                    </article>
                    <article class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="block-form block-order-total box-border wow fadeInRight" data-wow-duration="1s">
                            <h3><i class="fa fa-dollar"></i>Total</h3>
                            <hr>
                            <ul class="list-unstyled">
                                <li>Item(s): <strong>{{ Cart::count() }}</strong></li>
                                <li><hr></li>
                                <li><b>Total:</b> <strong>Rp. {{ Cart::subtotal() }}</strong></li>
                            </ul>
                        </div>
                    </article>
                    <article class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="block-form block-order-total box-border wow fadeInRight" data-wow-duration="1s">
                            <h3><i class="fa fa-bank"></i>PLEASE TRANSFER TO</h3>
                            <hr>
                            <ul class="list-unstyled">
                                <li> BANK NAME : <strong>BRI</strong></li>
                                <li><hr></li>
                                <li><b>No.Rek :</b> <strong>2061-01-005288-50-8</strong></li>
                                <li><b>Atas Nama :</b> <strong>Muhammad Ilham</strong>
                            </ul>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
