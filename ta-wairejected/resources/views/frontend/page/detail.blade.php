@extends('frontend.template')
@section('title', 'Home')
@section('content')
<section>
    <div class="second-page-container">
        <div class="container">
            <div class="row">
            <div class="header-for-light">
                        <h1 class="wow fadeInRight animated" data-wow-duration="1s">PRODUCT <span>DETAIL</span> </h1>
                    </div>
                    <div class="block-product-detail">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="product-image">
                                    <img id="product-zoom"  src="{{ url($data->product->product_image)}}" data-zoom-image="{{ url($data->product->product_image)}}" alt="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="product-detail-section">
                                    <h3>{{$data->product->product_title}}</h3>
                                    <div class="product-information">
                                        <div class="clearfix">
                                            <label class="pull-left">Product Code:</label>{{$data->product->product_id}}
                                        </div>
                                        <div class="clearfix">
                                            <label class="pull-left">Product Category:</label>{{$data->product->category_name}}
                                        </div>
                                        <div class="clearfix">
                                            <label class="pull-left">Size:</label>
                                            <p>{{$data->product->product_size}}</p>
                                        </div>
                                        <div class="clearfix">
                                            <label class="pull-left">Description:</label>
                                            <p>{{$data->product->product_description}}</p>
                                        </div>
                                        <div class="clearfix">
                                            <label class="pull-left">Availability:</label>
                                            <p>{{$data->product->product_stock}} in stock</p>
                                        </div>

                                        <div class="clearfix">
                                            <label class="pull-left">Price:</label>
                                            <p class="product-price">Rp {{$data->product->product_price}}</p>
                                        </div>{{--
                                        <div class="clearfix">
                                            <label class="pull-left">Total:</label>
                                            <p class="product-price">$850</p>
                                        </div>--}}
                                        <div class="shopping-cart-buttons">
                                            <a class="shoping"
                                                    onclick="event.preventDefault();
                                                    document.getElementById('cart-form{{$data->product->product_id}}').submit();">
                                                <i class="fa fa-shopping-cart"></i>  Add to cart </a>

                                            {{--<a href="#" class="shoping"><i class="fa fa-shopping-cart"></i></a>--}}
                                            <form id="cart-form{{$data->product->product_id}}" action="{{ route('cart.store',['id'=>$data->product->product_id]) }}" method="POST" style="display: none;">
                                                @csrf{{--
                                                <input type="hidden" name="id" value="{{ $data->product->product_id }}">--}}
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  
                </div>
                

            </div>
        </div>
    </div>

</section>

@endsection
