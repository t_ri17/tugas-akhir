@extends('frontend.template')
@section('title', 'artikel')
@section('content')
    <section>
    <div class="second-page-container">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="header-for-about">
                        <h1 class="wow fadeInRight animated" data-wow-duration="1s">Artikel <span> Wai Rejected</span></h1>
                    </div>
                    <div class="grid-container-artikel">
                        @foreach (@$data->artikel as $item)
                            <div class="grid-artikel"><img src="{{asset($item->artikel_image)}}">
                                <div style="color: #7c1212" class="grid-artikel-font"> {{$item->artikel_title}} </div>
                                <div > {{$item->artikel_des}}</div>

                                <div><a href="{{$item->artikel_url}}">Read More</a> </div></div>
                        @endforeach
                    </div>
    </section>
@endsection


