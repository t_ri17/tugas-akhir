@extends('frontend.template')
@section('title', 'Detail Order')
@section('content')
    <section>
        <div class="second-page-container">
            <div class="block">
                <div class="container">
                    <div class="header-for-light">
                        <h1 class="wow fadeInRight animated" data-wow-duration="1s">History <span>Detail</span></h1>
                    </div>
                    <div class="row">
                        <article class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                            <div class="box-border wow fadeInLeft" data-wow-duration="1s">
                                <h3><i class="fa fa-tasks"></i> History</h3>
                                <hr>
                                <div class="table-responsive">
                                    <table id="table-layout" class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Image</th>
                                            <th>Qty</th>
                                            <th>Price</th>
                                            <th>Price Total</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($data->od as $item)
                                            <tr>
                                                <td>{{$item->product_title}}</td>
                                                <td><img src="{{ url($item->product_image)}}" alt="Smiley face" height="42" width="42"></td>
                                                <td>{{$item->order_detail_qty}}</td>
                                                <td>{{$item->product_price}}</td>
                                                <td>{{$item->order_detail_price}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </article>
                        <article class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <div class="block-order-total box-border wow fadeInRight" data-wow-duration="1s">
                                <h3><i class="fa fa-dollar"></i> Total</h3>
                                <hr>
                                <ul class="list-unstyled">
                                    @if (!empty($data->his))
                                        <li>Order Date: <strong>{{ date('F d, Y', strtotime($data->his->created_at)) }}</strong></li>
                                        <li>Total counts: <strong>{{ !empty($data->his->cc) ? $data->his->cc : 0 }}</strong></li>
                                        <li>Total items: <strong>{{ !empty($data->his->i) ? $data->his->i : 0 }}</strong></li>
                                        <li class="color-active">Total spend: <strong>Rp. {{ !empty($data->his->s) ? $data->his->s : 0 }}</strong></li>
                                    @endif
                                </ul>
                            </div>
                        </article>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection
