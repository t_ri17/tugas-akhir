@extends('frontend.template')
@section('title', 'Home')
@section('content')
    @include('frontend.layout.slider')
    <section>
        <div class="block color-scheme-1">
            <div class="container">
                <div class="header-for-dark">
                    <h1 class="wow fadeInRight animated" data-wow-duration="1s">New  <span>Products</span></h1>
                    <div class="header-bottom-line"></div>
                    <div class="toolbar-for-dark" id="nav-bestseller">
                        <a href="javascript:;" data-role="prev" class="prev"><i class="fa fa-angle-left"></i></a>
                        <a href="javascript:;" data-role="next" class="next"><i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
                <div id="owl-bestseller"  class="owl-carousel">
                    @foreach(@$data->product as $item)
                    @if($item->product_stock>0)
                        <div class="text-center">
                            <article class="product dark">
                                <figure class="figure-hover-overlay" style="background-image: url({{ url($item->product_image) }}); background-size: cover; background-position: 50% 50%; width: 253px; height: 316px">
                                    <a href="#"  class="figure-href"></a>
                                    {{--<div class="product-new">new</div>--}}
                                    {{--<div class="product-sale">11% <br> off</div>--}}
                                    <a href="{{route('productDetail',['id'=>$item->product_id])}}" class="product-compare"><i class="fa fa-eye"></i></a>
                                    <img src="#" class="img-responsive" alt="">
                                </figure>
                                <div class="product-caption">
                                    <div class="block-name">
                                        <a href="#" class="product-name">{{ $item->product_title }}</a>
                                        <p class="product-price">Rp {{ $item->product_price }}</p>
                                    </div>
                                    <div class="category-title">
                                        <a href="{{route('category','cat='.$item->category_id)}}" class="review">{{ $item->category_name }}</a>
                                    </div>
                                    <div class="product-cart">
                                        <a
                                            onclick="event.preventDefault();
                                                     document.getElementById('cart-form{{$item->product_id}}').submit();">
                                            <i class="fa fa-shopping-cart"></i>
                                        </a>
                                        <form id="cart-form{{$item->product_id}}" action="{{ route('cart.store',['id'=>$item->product_id]) }}" method="POST" style="display: none;">
                                            @csrf
                                            {{--<input type="hidden" name="id" value="{{ $item->product_id }}">--}}
                                        </form>
                                    </div>
                                    <p class="description">{{ $item->product_description }}</p>
                                </div>
                            </article>
                        </div>
                        @endif
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

