@extends('frontend.template')
@section('title', 'Home')
@section('content')
    <section>
        <div class="second-page-container">
            <div class="block">
                <div class="container">
                    @if (Cart::count() > 0)
                    <div class="header-for-light">
                        <h1 class="wow fadeInRight animated" data-wow-duration="1s">Shopping<span> Cart</span></h1>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="cart-table table wow fadeInLeft" data-wow-duration="1s">
                                <thead>
                                <tr>
                                    <th class="card_product_image">Image</th>
                                    <th class="card_product_name">Product Name</th>
                                    <th class="card_product_quantity">Quantity</th>
                                    <th class="card_product_price">Unit Price</th>
                                    <th class="card_product_total">Total</th>
                                    <th class="card_product_action">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach (Cart::content() as $item)
                                        <tr>
                                            <td class="card_product_image" data-th="Image"><a href="#"><img
                                                        title="Product Name" alt="Product Name"
                                                        src="{{ url($item->options->img) }}"></a></td>
                                            <td class="card_product_name" data-th="Product Name"><a href="#">{{ $item->name }}</a></td>
                                            <td class="card_product_quantity" data-th="Quantity">

                                                <form id="cart-delete" action="{{ route('cart.update', ['id' => $item->rowId]) }}" method="POST">
                                                    {{csrf_field()}}{{method_field('POST')}}
                                                    <input type="number" min="1" value="{{ $item->qty }}" name="qty" class="styler">
                                                    <button type="submit">
                                                        <i class="fa fa-refresh"></i>
                                                    </button>
                                                </form>
                                            </td>



                                            <td class="card_product_price" data-th="Unit Price">Rp. {{ $item->price }}</td>
                                            <td class="card_product_total" data-th="Total">{{ $item->subtotal }}</td>
                                            <td class="card_product_action" data-th="Action"><a href="{{route('cart.delete', ['id' => $item->id])}}"
                                                                                                onclick="event.preventDefault();
                                                     document.getElementById('cart-delete').submit();">
                                                    <i class="fa fa-trash-o fa-2x"></i>
                                                </a>
                                                <form id="cart-delete" action="{{ route('cart.delete', ['id' => $item->rowId]) }}" method="POST" style="display: none;">
                                                    {{csrf_field()}}{{method_field('DELETE')}}
                                                </form></td>
                                        </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="row">
                            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="block-form block-order-total box-border wow fadeInRight"
                                     data-wow-duration="1s">
                                    <h3><i class="fa fa-dollar"></i> Total</h3>
                                    <hr>
                                    <ul class="list-unstyled">
                                        <li><b>Total:</b> <strong>Rp. {{ Cart::subtotal() }}</strong></li>
                                    </ul>
                                    <a href="{{ route('product') }}" class="btn btn-danger">Continue Shoping</a>
                                    <a href="{{ route('checkout.index') }}" class="btn-default-1">Checkout</a>
                                </div>
                            </article>
                        </div>
                    </div>

                    @else
                        <h1>No Item in Cart</h1>
                    <br>
                        <a href="{{ route('product') }}" class="btn btn-danger">Continue Shoping</a>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection
