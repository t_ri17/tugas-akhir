@extends('frontend.template')
@section('title', 'howtobuy')
@section('content')
    <section>
    <div class="second-page-container">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="header-for-light">
                        <h1 class="wow fadeInRight animated" data-wow-duration="1s">How<span> To Buy </span></h1>
                    </div>
                    <div class="left-kolom">
                    <div class="grid-htb"><img src="http://127.0.0.1:8000/effect/img/howtobuy/1.jpg">
                        <h3>1. Login dan Register</h3>
                    <li>Login Akun anda, Apabila anda tidak memiliki Akun silahkan Klik tulisan Register</li>
                    <li>akan masuk ke dalam form Register isi semua Biodata anda dengan lengkap</li></div>
                    <div class="grid-htb"><img src="http://127.0.0.1:8000/effect/img/howtobuy/2.jpg">
                        <h3>2. Memilih Produk Detail</h3>
                        <li>Pilih Produk apa yang anda ingin beli lalu klik Add to Cart seperti Gambar diatas</li>
                        <li>Produk pilihan anda secara otomatis akan masuk ke List Belanjaan anda/Shopping Cart</li></div>
                        <div class="grid-htb"><img src="http://127.0.0.1:8000/effect/img/howtobuy/3.jpg">
                            <h3>3. Shopping Cart</h3>
                            <li>Pada bagian atas kanan adalah list belanjaan anda, anda bisa melihat cart dan juga bisa langsung checkout untuk membayar</li>
                            <li>akan masuk ke dalam form Register isi semua Biodata anda dengan lengkap</li></div></div>

                    <div class="right-kolom">
                        <div class="grid-htb"><img src="http://127.0.0.1:8000/effect/img/howtobuy/4.jpg">
                            <h3>4. Checkout Detail</h3>
                            <li>Pada Form Checkout ini Customer diharuskan mengisi Form dengan Lengkap pada form checkout</li>
                            <li>Pada kanan atas adalah jumlah yang harus dibayarkan Customer</li>
                            <li>Pada Kanan bawah adalah No Rec Admin untuk melakukan Transfer</li></div>
                    <div class="grid-htb"><img src="http://127.0.0.1:8000/effect/img/howtobuy/5.jpg">
                        <h3>5. Order History</h3>
                        <li>Pada Form Order History adalah List dari Pembelian Produk Customer</li>
                        <li>Pada bagian Order History terdapat List Status Unpaid, Paid, Delivery, dan Arrived</li>
                        <li>Apabila Customer sudah membayar dan mensertakan bukti transferan, maka Admin akan mengubah status yang sebelumnya Unpaid menjadi Paid</li>
                        <li>Pada saat jam kerja admin akan mengirim Produk ke Lokasi tujuan/customer maka admin akan mengubah status menjadi Delivey, barang yang sudah sampai Adminakan mengubah menjadi Arrived</li></div>
                        <div class="grid-htb"><img src="http://127.0.0.1:8000/effect/img/howtobuy/6.jpg">
                            <h3>6. Send Message</h3>
                            <li>Untuk mempercepat Proses Customer diharapkan memberikan pesan ke Admin apabila suah melakukan transaksi supaya segera di ketahui oleh admin</li></div></div>
    </section>
@endsection


