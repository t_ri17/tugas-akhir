@extends('frontend.template')
@section('title', 'History')
@section('content')
    <section>
        <div class="second-page-container">
            <div class="block">
                <div class="container">
                    <div class="header-for-light">
                        <h1 class="wow fadeInRight animated" data-wow-duration="1s">Order <span>History</span></h1>
                    </div>
                    <div class="row">
                        <article class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                            <div class="box-border wow fadeInLeft" data-wow-duration="1s">
                                <h3><i class="fa fa-tasks"></i> History</h3>
                                <hr>
                                <div class="table-responsive">
                                    <table id="table-layout" class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>Order
                                                date
                                            </th>
                                            <th>No. of
                                                items
                                            </th>
                                            <th>Order
                                                total
                                            </th>
                                            <th>
                                                Status
                                            </th>
                                            <th>
                                                &nbsp;View
                                            </th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>

                            </div>
                        </article>
                        <article class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <div class="block-order-total box-border wow fadeInRight" data-wow-duration="1s">
                                <h3><i class="fa fa-dollar"></i> Total</h3>
                                <hr>
                                <ul class="list-unstyled">
                                    @if (!empty($data->his))
                                    <li>Customer since: <strong>{{ date('F d, Y', strtotime(Auth::user()->created_at)) }}</strong></li>
                                    <li>Total counts: <strong>{{ !empty($data->his->cc) ? $data->his->cc : 0 }}</strong></li>
                                    <li>Total items: <strong>{{ !empty($data->his->i) ? $data->his->i : 0 }}</strong></li>
                                    <li class="color-active">Total spend: <strong>Rp. {{ !empty($data->his->s) ? $data->his->s : 0 }}</strong></li>
                                    @endif
                                </ul>
                            </div>
                        </article>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>
        var table = $('#table-layout').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('history.get',['p'=>'from_id']) }}",
            columns: [
                {data: 'created_at', name: 'created_at'},
                {data: 'order_items', name: 'order_items'},
                {data: 'order_price', name: 'order_price'},
                {data: 'status', name: 'status'},
                {data: 'action', name: 'action', searchable: false, orderable: false}
            ],
        });

        function isdelete(k) {
            swal({
                    title: "Are you sure?",
                    text: "Data Akan Terhapus!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#fb483a',
                    confirmButtonText: "Yes, delete it!"
                },
                function(){
                    $('#delete-form'+k).submit();
                });
        }
    </script>
@endsection
