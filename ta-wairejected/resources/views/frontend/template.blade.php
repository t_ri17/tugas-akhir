<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">``
    <![endif]-->
    <title>Wai Rejected | @yield('title')</title>
    <meta name="description" content="Effect, premium HTML5&amp;CSS3 template">
    <meta name="author" content="MosaicDesign">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="{{ url('effect/css/theme-style.css') }}">
    <link rel="stylesheet" href="{{ url('effect/css/ie.style.css') }}">
    <link rel="stylesheet" href="{{ url('bootstrap/css/bootstrap.min.css') }}">
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js') }}"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js') }}"></script>
    <![endif]-->
    <!--[if IE 7]>
    <link rel="stylesheet" href="{{ url('effect/css/font-awesome-ie7.css') }}">
    <![endif]-->
    <link rel="stylesheet" href="{{ url('effect/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ url('effect/css/sweetalert.min.css') }}">
    <script src="{{ url('effect/js/vendor/modernizr.js') }}"></script>
    <!--[if IE 8]><script src="{{ url('effect/js/vendor/less-1.3.3.js') }}"></script><![endif]-->
    <!--[if gt IE 8]><!--><script src="{{ url('effect/js/vendor/less.js') }}"></script><!--<![endif]-->

    @yield('css')
</head>
<body>
@include('frontend.layout.navbar')

@yield('content')

@include('frontend.layout.footer')
<script src="{{ url('effect/js/vendor/jquery.js') }}"></script>
<script src="{{ url('effect/js/vendor/jquery.easing.1.3.js') }}"></script>
{{--<script src="{{ url('effect/js/vendor/bootstrap.js') }}"></script>--}}
<script src="{{ url('bootstrap/js/bootstrap.min.js') }}"></script>

<script src="{{ url('effect/js/vendor/jquery.flexisel.js') }}"></script>
<script src="{{ url('effect/js/vendor/wow.min.js') }}"></script>
<script src="{{ url('effect/js/vendor/jquery.transit.js') }}"></script>
<script src="{{ url('effect/js/vendor/jquery.jcountdown.js') }}"></script>
<script src="{{ url('effect/js/vendor/jquery.jPages.js') }}"></script>
<script src="{{ url('effect/js/vendor/owl.carousel.js') }}"></script>

<script src="{{ url('effect/js/vendor/responsiveslides.min.js') }}"></script>
<script src="{{ url('effect/js/vendor/jquery.elevateZoom-3.0.8.min.js') }}"></script>

<!-- jQuery REVOLUTION Slider  -->
<script type="text/javascript" src="{{ url('effect/js/vendor/jquery.themepunch.plugins.min.js') }}"></script>
<script type="text/javascript" src="{{ url('effect/js/vendor/jquery.themepunch.revolution.min.js') }}"></script>
<script type="text/javascript" src="{{ url('effect/js/vendor/jquery.scrollTo-1.4.2-min.js') }}"></script>

<!-- Custome Slider  -->
<script type="text/javascript" src="{{ url('effect/js/vendor/lightbox.js')}}"></script>
<script type="text/javascript" src="{{ url('effect/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ url('effect/js/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript" src="{{ url('effect/js/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ url('effect/js/bootstrap-notify.min.js') }}"></script>

{{--<script src="{{ url('effect/js/jquery.magnific-popup.js') }}"></script>--}}


<script src="{{ url('effect/js/main.js') }}"></script>
<script>
    @if(session()->has('success'))
    $.notify({
        // options
        message: '{{ session()->get('success') }}'
    },{
        // settings
        type: 'success'
    });
    @elseif(session()->has('error'))
    $.notify({
        // options
        message: '{{ session()->get('error') }}'
    },{
        // settings
        type: 'danger'
    });
    @endif

    //<![CDATA[
    $('#myModal').on('shown.bs.modal', function (event) {
        $(this).find('.modal-body').html( '<img src=" '+ $(event.relatedTarget).find('img').attr('src') +' " alt="" style="width: 100%;">' );
    })
    $('#myModal').on('hidden.bs.modal', function (event) {
        $(this).find('.modal-body').html( '' );
    })
    //]]>
</script>
@yield('js')
<!--Here will be Google Analytics code from BoilerPlate-->
</body>
</html>
