<!-- Header-->
<header id="header">
    <div class="header-bg">
        <div class="header-main" id="header-main-fixed">
            <div class="header-main-block1">
                <div class="container">
                    <div id="container-fixed">
                        <div class="row">
                            <div class="col-md-3">
                                <a href="/" class="header-logo"> <img src="{{ url('/effect/img/logo-big-shop.png') }}" alt=""></a>
                            </div>
                            <div class="col-md-5">
                                <div class="top-search-form pull-left">
                                    <form action="{{route('artikel')}}" >
                                        <input type="search" placeholder="Search ..." name="q" class="form-control">
                                        <button type="submit"><i class="fa fa-search"></i></button>
                                    </form>
                                </div>
                            </div>
                            <div id="wishlist" class="col-md-4">
                                <div class="header-mini-cart  pull-right">
                                    <a href="#"  data-toggle="dropdown">
                                        Shopping cart
                                        @if (Cart::count() > 0)
                                            <span>{{Cart::count()}} item(s)-Rp {{Cart::subtotal()}}</span>
                                        @else<span>0 item(s)-0.00</span>@endif
                                    </a>
                                    <div class="dropdown-menu shopping-cart-content pull-right">
                                        <div class="shopping-cart-items">
                                            @if (Cart::count() > 0)
                                                @foreach (Cart::content() as $item)
                                                    <div class="item pull-left">
                                                        <img src="{{ url($item->options->img) }}" alt="Product Name" class="pull-left" width="70px" height="auto">


                                                        <div class="pull-left">
                                                            <p>{{ $item->name }}</p>
                                                            <p>Rp. {{ $item->price }}&nbsp;<strong>x {{ $item->qty }}</strong></p>
                                                        </div>
                                                        <a href="{{route('cart.delete', ['id' => $item->id])}}"
                                                           onclick="event.preventDefault();
                                                     document.getElementById('cart-delete').submit();" class="trash">
                                                            <i class="fa fa-trash-o pull-left"></i>
                                                        </a>
                                                        <form id="cart-delete" action="{{ route('cart.delete', ['id' => $item->rowId]) }}" method="POST" style="display: none;">
                                                            {{csrf_field()}}{{method_field('DELETE')}}
                                                        </form>
                                                    </div>
                                                @endforeach
                                            @endif
                                            <div class="total pull-left">
                                                <table>
                                                    <tbody class="pull-right">
                                                    <tr class="color-active">
                                                        <td><b>Total:</b></td>
                                                        <td><b>Rp. {{ Cart::subtotal() }}</b></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <a href="{{ route('checkout.index') }}" class="btn-read pull-right">Checkout</a>
                                                <a href="{{ route('cart.index') }}" class="btn-read pull-right">View Cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- /header-mini-cart -->
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        
            <div class="header-main-block2">
                <nav class="navbar yamm  navbar-main" role="navigation">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                            <a href="/" class="navbar-brand"><i class="fa fa-home"></i></a>
                        </div>
                        <div id="navbar-collapse-1" class="navbar-collapse collapse ">
                            <ul class="nav navbar-nav">
                                <!-- Classic list -->
                                <li>
                                    <a  href="{{ route('music') }}">Music</a>
                                </li>
                                <li>
                                    <a  href="{{ route('photo') }}">Photo</a>
                                </li>
                                <li>
                                    <a  href="{{ route('artikel') }}">Artikel</a>
                                </li>

                                <li>
                                    <a  href="{{ route('vidio') }}">Video</a>
                                </li>
                                <li>
                                    <a  href="{{ route('category') }}">Category</a>
                                </li>
                                <li>
                                    <a href="{{ route('product') }}">Product</a>
                                </li>
                                <li>
                                    <a  href="{{ route('about') }}">About</a>
                                </li>

                            </ul>

                            <ul class="nav navbar-nav navbar-right">
                                @guest
                                <li><a href="{{ route('login') }}">Login</a></li>
                                <li><a href="{{ route('register') }}">Register</a></li>
                                @else
                                <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">{{ Auth::user()->name }} <i class="fa fa-caret-right fa-rotate-45"></i></a>
                                    <ul class="dropdown-menu list-unstyled fadeInUp animated">
                                        <li><a href="{{ route('inbox.index') }}"><i class="fa fa-envelope"></i>&nbsp; Message</a></li>
                                        <li><a href="{{ route('checkout.index') }}"><i class="fa fa-check"></i>&nbsp; Checkout</a></li>
                                        <li><a href="{{ route('cart.index') }}"><i class="fa fa-shopping-cart"></i>&nbsp; Cart</a></li>
                                        <li><a href="{{ route('history.index') }}"><i class="fa fa-tasks"></i>&nbsp; History</a></li>
                                        <li>
                                            <a href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                <i class="fa fa-sign-out "></i>&nbsp;{{ __('Logout') }}
                                            </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                                @endguest
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>

        <!-- /header-main-menu -->
    </div>
</header>
<!-- End header -->
