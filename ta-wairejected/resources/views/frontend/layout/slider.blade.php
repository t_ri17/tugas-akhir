<section>
    <div class="revolution-container">
        <div class="revolution">
            <ul class="list-unstyled">	<!-- SLIDE  -->
                <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
                    <img src="{{ url('effect/img/bg.jpg') }}"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                </li>
            </ul>
            <div class="revolutiontimer"></div>
        </div>
    </div>
</section>
