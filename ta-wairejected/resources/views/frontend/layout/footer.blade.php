
<footer id="footer-block">

    <div class="block color-scheme-dark-90">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <article class="payment-service dark">
                        <a href="{{ route('howtobuy') }}"></a>
                        <div class="row">
                            <div class="col-md-4 text-center">
                                <i class="fa fa-warning"></i>
                            </div>
                            <div class="col-md-8">
                            <br>
                                <h3 class="color-active">How to Buy</h3>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="col-md-4">
                    <article class="payment-service dark">
                        <a href="{{ route('faq') }}"></a>
                        <div class="row">
                            <div class="col-md-4 text-center">
                                <i class="fa fa-question"></i>
                            </div>
                            <div class="col-md-8">
                            <br>
                                <h3 class="color-active">F.A.Q</h3>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="col-md-4">
                    <article class="payment-service dark">
                        <a href="{{ route('contact') }}"></a>
                        <div class="row">
                            <div class="col-md-4 text-center">
                                <i class="fa fa-fax"></i>
                            </div>
                            <div class="col-md-8">
                                <br>
                                <h3 class="color-active">Contact</h3>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-information">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    </div>
                </div>
                <div class="container">
                <div class="row">
                <div class="col-md-3">
                    </div>
                </div>
                </div>      
            </div>
        </div>
        </div>
        </div>
        </div>
        </div>
    <div class="footer-copy color-scheme-1">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <a href="index.html" class="logo-copy pull-left"></a>
                </div>
                <div class="col-md-4">
                    <p  class="text-center">

                        <a href="https://www.twitter.com/WaiRejected/" target="_blank"><i class="fa fa-twitter fa-100x" style="font-size:40px"></i></a>
                        <a href="https://id-id.facebook.com/wairejected/" target="_blank"><i class="fa fa-facebook-square fa-100x" style="font-size:40px"></i></a>
                        <a href="https://www.instagram.com/wairejected/" target="_blank"><i class="fa fa-instagram" style="font-size:40px"></i></a>
                        <a href="https://www.youtube.com/channel/UCkMuF2g21b8m3izU40HaYkQ" target="_blank"><i class="fa fa-youtube-play" style="font-size:40px"></i></a>
                        <a href="https://open.spotify.com/artist/5rHYaJk7IfXRFOnnZ7aGID" target="_blank"><i class="fa fa-spotify fa-100x" style="font-size:40px"></i></a>
                        <a href="https://www.google.co.id/maps/place/WaiRejected/@-0.0392136,109.3142585,17z/data=!3m1!4b1!4m5!3m4!1s0x2e1d5954ae08cecd:0x1b7ea2dccd9f6341!8m2!3d-0.0392136!4d109.3164472" target="_blank"><i class="fa fa-map-marker" style="font-size:30px"></i></a>
                        <br>
                    <a href="#"> Copyright © 2019</a> Design By | Tri Jayanto  | All rights reserved. </a>
                    </p>
                </div>

            </div>
        </div>
    </div>
</footer>
<!-- End Section footer -->
