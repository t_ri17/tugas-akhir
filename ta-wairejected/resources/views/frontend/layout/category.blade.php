<aside class="col-md-3">
    <div class="main-category-block ">
        <div class="main-category-title">
            <i class="fa fa-list"></i> Category

        </div>
    </div>
    <div class="widget-block">
        <ul class="list-unstyled ul-side-category">
            <li>
                @foreach($data->category as $item)
                    <a href="{{route('category','cat='.$item->category_id)}}"><i class="fa fa-angle-right"></i> {{ $item->category_name }} / {{ $item->cc }}</a>
                @endforeach
            </li>
        </ul>

    </div>
</aside>
