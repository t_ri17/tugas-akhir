<div id="alert" class="row ml-auto pull-right" data-wow-duration="1s" style="position:absolute; top: 10px !important; right: 25px !important;z-index: 123123123;">
    <div class="alert-group" style="width:100%">
        <div class="alert alert-{{$class}} alert-dismissable" style="opacity: 0.7;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ $message }}
        </div>
    </div>
</div>

