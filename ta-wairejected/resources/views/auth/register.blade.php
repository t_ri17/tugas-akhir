@extends('frontend.template')

@section('content')
    <section>
        <div class="second-page-container">
            <div class="block">
                <div class="container">
                    <div class="header-for-light">
                        <h1 class="wow fadeInRight animated" data-wow-duration="1s">Create new <span>Account</span></h1>
                    </div>
                    <div class="row">
                         <article class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                            <div class="block-form box-border wow fadeInLeft animated" data-wow-duration="1s">
                                <h3><i class="fa fa-user"></i>Personal Information</h3>
                                <hr>

                                <form class="form-horizontal" role="form" method="post" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                                    @csrf
                                    <div class="form-group">
                                        <label for="inputLastName" class="col-sm-3 control-label">Full Name:<span class="text-error">*</span></label>
                                        <div class="col-sm-9">
                                            <input id="inputLastName" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required>
                                            @if ($errors->has('name'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEMail" class="col-sm-3 control-label">E-Mail:<span class="text-error">*</span></label>
                                        <div class="col-sm-9">
                                            <input id="inputEMail" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPhone" class="col-sm-3 control-label">Phone:</label>
                                        <div class="col-sm-9">
                                            <input id="inputPhone" type="text" class="form-control{{ $errors->has('user_phone') ? ' is-invalid' : '' }}" name="user_phone" value="{{ old('user_phone') }}" required>
                                            @if ($errors->has('user_phone'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('user_phone') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword1" class="col-sm-3 control-label">Password: <span class="text-error">*</span></label>
                                        <div class="col-sm-9">
                                            <input type="password" class="form-control" id="inputPassword1" name="password" required>
                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword2" class="col-sm-3 control-label">Re-Password: <span class="text-error">*</span></label>
                                        <div class="col-sm-9">
                                            <input type="password" class="form-control" id="inputPassword2" name="password_confirmation" required>
                                            @if ($errors->has('password-confirm'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password-confirm') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            <button type="submit" class="btn-default-1">Register</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

