<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">``
    <![endif]-->
    <title> Wai Rejected | @yield('title')</title>
    <meta name="description" content="Effect, premium HTML5&amp;CSS3 template">
    <meta name="author" content="MosaicDesign">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="{{ url('effect/css/theme-style.css') }}">
    <link rel="stylesheet" href="{{ url('effect/css/ie.style.css') }}">
    <link rel="stylesheet" href="{{ url('effect/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ url('effect/css/sweetalert.min.css') }}">
    <script src="{{ url('effect/js/vendor/modernizr.js') }}"></script>
    @yield('css')
</head>
<body class="body-bg">
<section>
    <div class="">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="main-category-block ">
                        <div class="main-category-title">
                            <i class="fa fa-list"></i> Main Menu
                        </div>
                        <nav class="main-category-items">
                            <ul class="main-category-list list-unstyled">
                                <li>
                                    <a href="{{ route('admin.dashboard.index') }}"><i class="fa fa-home"></i>Dashboard</a>
                                </li>
                                <li class="with-dropdown collapsed" data-toggle="collapse" data-target="#artikel">
                                    <a href="#"><i class="fa fa-dropbox"></i>Artikel</a>
                                    <ul id="artikel" class="list-unstyled collapse">
                                        <li>
                                            <a  href="{{ route('admin.artikel.index') }}">Artikel list </a>
                                        </li>
                                        <li>
                                            <a  href="{{ route('admin.artikel.form') }}">Create Artikel </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="with-dropdown collapsed" data-toggle="collapse" data-target="#photo">
                                <a href="#"><i class="fa fa-photo"></i>Photo</a>
                                <ul id="photo" class="list-unstyled collapse">
                                    <li>
                                        <a  href="{{ route('admin.photo.index') }}">Photo list </a>
                                    </li>
                                    <li>
                                        <a  href="{{ route('admin.photo.form') }}">Create list </a>
                                    </li>
                                </ul>
                                </li>
                                <li class="with-dropdown collapsed" data-toggle="collapse" data-target="#music">
                                <a href="#"><i class="fa fa-music"></i>Music</a>
                                <ul id="music" class="list-unstyled collapse">
                                    <li>
                                        <a  href="{{ route('admin.music.index') }}">Music list </a>
                                    </li>
                                    <li>
                                        <a  href="{{ route('admin.music.form') }}">Create list </a>
                                    </li>
                                </ul>
                                </li>
                                <li class="with-dropdown collapsed" data-toggle="collapse" data-target="#user">
                                    <a href="#"><i class="fa fa-users"></i>User</a>
                                    <ul id="user" class="list-unstyled collapse">
                                        <li>
                                            <a  href="{{ route('admin.user.index') }}">Users Data </a>
                                        </li>
                                        <li>
                                            <a  href="{{ route('admin.user.form') }}"> Create User </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="with-dropdown collapsed" data-toggle="collapse" data-target="#category">
                                    <a href="#"><i class="fa fa-tags"></i>Category</a>
                                    <ul class="list-unstyled collapse" id="category">
                                        <li>
                                            <a  href="{{ route('admin.category.index') }}"> Category Data </a>
                                        </li>
                                        <li>
                                            <a  href="{{ route('admin.category.form') }}"> Create Category </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="with-dropdown collapsed" data-toggle="collapse" data-target="#product">
                                    <a href="#"><i class="fa fa-print"></i>Product</a>
                                    <ul class="list-unstyled collapse" id="product">
                                        <li>
                                            <a href="{{ route('admin.product.index') }}">Product Data </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('admin.product.form') }}">Product Create </a>
                                        </li>
                                    </ul>

                                </li>

                                <li class="with-dropdown collapsed" data-toggle="collapse" data-target="#order">
                                    <a href="#"><i class="fa fa-shopping-cart"></i>Order</a>
                                    <ul class="list-unstyled collapse" id="order">
                                        <li>
                                            <a href="{{ route('admin.order.index') }}">Order </a>
                                        </li>
                                     {{--   <li>
                                            <a href="{{ route('admin.order.report') }}">Report </a>
                                        </li>
                                    --}}</ul>
                                </li>
                                <li class="with-dropdown collapsed" data-toggle="collapse" data-target="#message">
                                    <a href="#"><i class="fa fa-envelope"></i>Message</a>
                                    <ul class="list-unstyled collapse" id="message">
                                        <li>
                                            <a href="{{ route('admin.inbox.form') }}">Send New Message </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('admin.inbox.index',['p' => 'from_id']) }}">Inbox Message </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('admin.inbox.index',['p' => 'to_id']) }}">Message Send </a>
                                        </li>
                                    </ul>

                                </li>
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="fa fa-sign-out "></i>&nbsp;{{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="home-slider-block">
                            <div class="block color-scheme-white-90">
                                <div class="container-fluid">
                                    <div class="header-for-light">
                                        <h1 class="wow fadeInRight animated animated" data-wow-duration="1s" style="visibility: visible;-webkit-animation-duration: 1s; -moz-animation-duration: 1s; animation-duration: 1s;">@yield('title') <span>@yield('sub_title')</span></h1>
                                    </div>
                                    @yield('content')
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@include('frontend.layout.footer')
<script src="{{ url('effect/js/vendor/jquery.js') }}"></script>
<script src="{{ url('effect/js/vendor/jquery.easing.1.3.js') }}"></script>
<script src="{{ url('effect/js/vendor/bootstrap.js') }}"></script>

<script src="{{ url('effect/js/vendor/jquery.flexisel.js') }}"></script>
<script src="{{ url('effect/js/vendor/wow.min.js') }}"></script>
<script src="{{ url('effect/js/vendor/jquery.transit.js') }}"></script>
<script src="{{ url('effect/js/vendor/jquery.jcountdown.js') }}"></script>
<script src="{{ url('effect/js/vendor/jquery.jPages.js') }}"></script>
<script src="{{ url('effect/js/vendor/owl.carousel.js') }}"></script>

<script src="{{ url('effect/js/vendor/responsiveslides.min.js') }}"></script>
<script src="{{ url('effect/js/vendor/jquery.elevateZoom-3.0.8.min.js') }}"></script>

<!-- jQuery REVOLUTION Slider  -->
<script type="text/javascript" src="{{ url('effect/js/vendor/jquery.themepunch.plugins.min.js') }}"></script>
<script type="text/javascript" src="{{ url('effect/js/vendor/jquery.themepunch.revolution.min.js') }}"></script>
<script type="text/javascript" src="{{ url('effect/js/vendor/jquery.scrollTo-1.4.2-min.js') }}"></script>

<!-- Custome Slider  -->

<script type="text/javascript" src="{{ url('effect/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ url('effect/js/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript" src="{{ url('effect/js/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ url('effect/js/bootstrap-notify.min.js') }}"></script>
<script src="{{ url('effect/js/main.js') }}"></script>
<script>
    @if(!empty($data->l))
    $.notify('{{$data->l[1]}}', {
            type : '{{$data->l[0]}}',
            animate: {
                enter: "animated fadeInUp",
                exit: "animated fadeOutDown"
            }
        }
    );@endif
</script>
@yield('js')
<!--Here will be Google Analytics code from BoilerPlate-->
</body>
</html>
