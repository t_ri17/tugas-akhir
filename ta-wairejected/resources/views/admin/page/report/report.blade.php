@extends('admin.template')
@section('title', 'Data Report')
@section('sub_title', '')
@section('menu','Data Report')
@section('content')
    <table id="table-layout" class="table table-bordered table-striped" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>No</th>
            <th>Date</th>
            <th>Product Name</th>
            <th>Qty</th>
            <th>Total</th>
            <th class="" style="text-align: center; width: 20%;"><span class="fa fa-ellipsis-v "></span></th>
        </tr>
        </thead>
        <tbody>
        @if(!empty($data->report))
            @foreach($data->report as $item)
                <tr>
                    <td>{{++$data->count}}</td>
                    <td>{{date('d-M-Y', strtotime($item->oc))}}</td>
                    <td>{{$item->product_title}}</td>
                    <td>{{$item->order_detail_qty}}</td>
                    <td>Rp {{$item->order_price}},00</td>
                    <td>
                        <div class="text-center">
                            <div class="btn-group">
                                <form id="delete-form{{$item->order_detail_id}}" action="{{route('admin.orderdetail.delete',['id'=>$item->order_detail_id])}}" method="POST" style="display: inline;" role="form">
                                    @csrf @method('DELETE')
                                    <button  onClick="isdelete({{$item->order_detail_id}})" type="button" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
                                </form>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach()
        @endif
        </tbody>
    </table>
@endsection
@section('js')
    <script src="//cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script>
        var table = $('#table-layout').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'print'
            ]
        });
        function isdelete(k) {
            swal({
                    title: "Are you sure?",
                    text: "Data Akan Terhapus!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#fb483a',
                    confirmButtonText: "Yes, delete it!"
                },
                function(){
                    $('#delete-form'+k).submit();
                });
        }
    </script>
@endsection
