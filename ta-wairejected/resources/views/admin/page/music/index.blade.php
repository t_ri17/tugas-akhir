@extends('admin.template')
@section('title', 'Data')
@section('sub_title', 'Music')
@section('menu','Main Menu')
@section('content')
    <table id="table-layout" class="table table-bordered table-striped" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name Album</th>
            <th>Nama Music</th>
            <th>List Music</th>
            <th>Image</th>

            <th class="" style="text-align: center; width: 20%;"><span class="fa fa-ellipsis-v "></span></th>
        </tr>
        </thead>
    </table>
@endsection
@section('js')
    <script>
        var table = $('#table-layout').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('admin.music.get') }}",
            columns: [
                {data: 'music_id', name: 'music_id'},
                {data: 'album_title', name: 'album_title'},
                {data: 'music_title', name: 'music_title'},
                {data: 'music_des', name: 'music_des'},
                {data: 'url', name: 'url'},
                {data: 'action', name: 'action', searchable: false, orderable: false}
            ],
        });

        function isdelete(k) {
            swal({
                    title: "Are you sure?",
                    text: "Data Akan Terhapus!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#fb483a',
                    confirmButtonText: "Yes, delete it!"
                },
                function(){
                    $('#delete-form'+k).submit();
                });
        }
    </script>
@endsection
