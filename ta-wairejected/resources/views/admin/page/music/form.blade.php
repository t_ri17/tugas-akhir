@extends('admin.template')
@section('title',  !empty($data->music) ? 'Edit' : 'Create')
@section('sub_title', 'Music')
@section('menu','Main Menu')
@section('content')
    <section>

        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="block-form box-border wow fadeInLeft animated" data-wow-duration="1s">
                <h3><i class="fa fa-tags"></i>Music Form</h3>
                <hr>
                <form class="form-horizontal" role="form" method="post" action="{{ !empty($data) ? route('admin.music.update', ['id' => $data->music_id]) : route('admin.music.post') }}" enctype="multipart/form-data">
                    @csrf{{--
                    <input type="hidden" name="music_id" value="{{ $data->music_id }}">--}}
                    <div class="form-group">
                        <label for="title" class="col-sm-3 control-label">Album Name:<span class="text-error">*</span></label>
                        <div class="col-sm-9">
                            <input  id="title" type="text" class="form-control{{ $errors->has('album_title') ? ' is-invalid' : '' }}" name="album_title" value="{{ @$data->album_title ? @$data->album_title :old('music_title') }}" required>
                            @if ($errors->has('album_title'))
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('album_title') }}</strong>
                        </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="title" class="col-sm-3 control-label">Music Name:<span class="text-error">*</span></label>
                        <div class="col-sm-9">
                            <input id="title" type="text" class="form-control{{ $errors->has('music_title') ? ' is-invalid' : '' }}" name="music_title" value="{{ @$data->music_title ? @$data->music_title :old('music_title') }}" required>
                            @if ($errors->has('music_title'))
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('music_title') }}</strong>
                        </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="title" class="col-sm-3 control-label">Music URL:<span class="text-error">*</span></label>
                        <div class="col-sm-9">
                            <input id="title" type="text" class="form-control{{ $errors->has('music_url') ? ' is-invalid' : '' }}" name="music_url" value="{{ @$data->music_url ? @$data->music_url :old('music_url') }}" required>
                            @if ($errors->has('music_url'))
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('music_url') }}</strong>
                        </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">

                        <label for="file" class="col-sm-3 control-label">Album Image:<span class="text-error">*</span></label>
                        <div class="col-sm-9">
                            @if(@$data->album_image)
                                <img src="{{url($data-> album_image)}}" alt="" id="img" height="120px">
                                <br>
                            @endif
                            <input id="file" type="file" name="file" class="form-control">
                            @if ($errors->has('file'))
                                <span class="help-block" style="color: red">
                                    <strong>{{ $errors->first('file') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="desc" class="col-sm-3 control-label">Music Description:<span class="text-error">*</span></label>
                        <div class="col-sm-9">
                            <textarea id="desc" class="form-control" name="music_des" required>{{ @$data->music_des ? @$data->music_des :old('music_des') }} </textarea>
                            @if ($errors->has('music_des'))
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('music_des') }}</strong>
                        </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn-default-1">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </article>

    </section>
@endsection
