@extends('admin.template')
@section('title',  !empty($data->product) ? 'Edit' : 'Create')
@section('sub_title', 'Product')
@section('menu','Main Menu')
@section('content')
    <section>

        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="block-form box-border wow fadeInLeft animated" data-wow-duration="1s">
                <h3><i class="fa fa-tags"></i>Product Form</h3>
                <hr>

                <form class="form-horizontal" role="form" method="post" action="{{ !empty($data->product) ? route('admin.product.update', ['id' => $data->product->product_id]) : route('admin.product.post') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="title" class="col-sm-3 control-label">Product Name:<span class="text-error">*</span></label>
                        <div class="col-sm-9">
                            <input id="title" type="text" class="form-control{{ $errors->has('product_title') ? ' is-invalid' : '' }}" name="product_title" value="{{ @$data->product->product_title ? @$data->product->product_title :old('product_title') }}" required>
                            @if ($errors->has('product_title'))
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('product_title') }}</strong>
                        </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="category" class="col-sm-3 control-label">Product Category:<span class="text-error">*</span></label>
                        <div class="col-sm-9">
                            <select name="product_category_id" class="form-control" required>
                                <option>Pilih Kategori</option>
                                @foreach(@$data->category as $item)
                                    <option value="{{@$item->category_id}}" {{old('product_category_id') == @$item->category_id ? "selected" : @$item->category_id == @$data->product->product_category_id ? 'selected' : null}}>{{@$item->category_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="size" class="col-sm-3 control-label">Product Size:<span class="text-error">*</span></label>
                        <div class="col-sm-9">
                            <input id="size" type="text" class="form-control{{ $errors->has('product_size') ? ' is-invalid' : '' }}" name="product_size" value="{{ @$data->product->product_size ? @$data->product->product_size :old('product_size') }}" required>
                            @if ($errors->has('product_size'))
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('product_size') }}</strong>
                        </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="desc" class="col-sm-3 control-label">Product Description:<span class="text-error">*</span></label>
                        <div class="col-sm-9">
                            <textarea id="desc" class="form-control" name="product_description" required>{{ @$data->product->product_description ? @$data->product->product_description :old('product_description') }} </textarea>
                            @if ($errors->has('product_description'))
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('product_description') }}</strong>
                        </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="price" class="col-sm-3 control-label">Product Price:<span class="text-error">*</span></label>
                        <div class="col-sm-9">
                            <input id="price" type="text" class="form-control{{ $errors->has('product_price') ? ' is-invalid' : '' }}" name="product_price" value="{{ @$data->product->product_price ? @$data->product->product_price :old('product_price') }}" required>
                            @if ($errors->has('product_price'))
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('product_price') }}</strong>
                        </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="stock" class="col-sm-3 control-label">Product Stock:<span class="text-error">*</span></label>
                        <div class="col-sm-9">
                            <input id="stock" type="text" class="form-control{{ $errors->has('product_stock') ? ' is-invalid' : '' }}" name="product_stock" value="{{ @$data->product->product_stock ? @$data->product->product_stock :old('product_stock') }}" required>
                            @if ($errors->has('product_stock'))
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('product_stock') }}</strong>
                        </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="file" class="col-sm-3 control-label">Product Image:<span class="text-error">*</span></label>
                        <div class="col-sm-9">
                        @if(@$data->product->product_image)
                            <img src="{{url($data->product->product_image)}}" alt="" id="img" height="120px">
                            <br>
                        @endif
                        <input id="file" type="file" name="file" class="form-control">
                        @if ($errors->has('file'))
                            <span class="help-block" style="color: red">
                                    <strong>{{ $errors->first('file') }}</strong>
                                </span>
                        @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn-default-1">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </article>

    </section>
@endsection

