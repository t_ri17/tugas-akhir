@extends('admin.template')
@section('title', 'Data')
@section('sub_title', 'Product')
@section('menu','Main Menu')
@section('content')
    <table id="table-layout" class="table table-bordered table-striped" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Category</th>
            <th>Image</th>
            <th>Price</th>
            <th>Stock</th>
            <th class="" style="text-align: center; width: 20%;"><span class="fa fa-ellipsis-v "></span></th>
        </tr>
        </thead>
    </table>
@endsection
@section('js')
    <script>
        var table = $('#table-layout').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('admin.product.get') }}",
            columns: [
                {data: 'product_id', name: 'product_id'},
                {data: 'product_title', name: 'product_title'},
                {data: 'category_name', name: 'category_name'},
                {data: 'url', name: 'url'},
                {data: 'product_price', name: 'product_price'},
                {data: 'product_stock', name: 'product_stock'},
                {data: 'action', name: 'action', searchable: false, orderable: false}
            ],
        });

        function isdelete(k) {
            swal({
                    title: "Are you sure?",
                    text: "Data Akan Terhapus!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#fb483a',
                    confirmButtonText: "Yes, delete it!"
                },
                function(){
                    $('#delete-form'+k).submit();
                });
        }
    </script>
@endsection
