@extends('admin.template')
@section('title', $data->title)
@section('sub_title', 'Message')
@section('menu','Main Menu')
@section('content')
    <table id="table-layout" class="table table-bordered table-striped" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>ID</th>
            @if($p === 'from_id')
                <th>From</th>
            @else
                <th>Message To</th>
            @endif
            <th>Message</th>
            <th class="" style="text-align: center; width: 20%;"><span class="fa fa-ellipsis-v "></span></th>
        </tr>
        </thead>
    </table>
@endsection
@section('js')
    <script>
        var table = $('#table-layout').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('admin.inbox.get',['p' =>  $p]) }}",
            columns: [
                {data: 'inbox_id', name: 'inbox_id'},
                {data: 'name', name: 'name'},
                {data: 'inbox_text', name: 'inbox_text'},
                {data: 'action', name: 'action', searchable: false, orderable: false}
            ],
        });

        function isdelete(k) {
            swal({
                    title: "Are you sure?",
                    text: "Data Akan Terhapus!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#fb483a',
                    confirmButtonText: "Yes, delete it!"
                },
                function(){
                    $('#delete-form'+k).submit();
                });
        }
    </script>
@endsection
