@extends('admin.template')
@section('title', 'Send New')
@section('sub_title', 'Message')
@section('menu','Main Menu')
@section('content')
    <section>

        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="block-form box-border wow fadeInLeft animated" data-wow-duration="1s">
                <h3><i class="fa fa-tags"></i>Message Form</h3>
                <hr>

                <form class="form-horizontal" role="form" method="post" action="{{ route('admin.inbox.post') }}">
                    @csrf
                    <div class="form-group">
                        <label for="to_id" class="col-sm-3 control-label">Send Message For:<span class="text-error">*</span></label>
                        <div class="col-sm-9">
                            <select id="to_id" name="inbox_to_id" class="form-control" required>
                                <option value="">Choose Receiver</option>
                                    @foreach(@$data->user as $item)
                                        @if($item->id !== Auth::user()->id)
                                            <option value="{{@$item->id}}" {{old('inbox_to_id') == @$item->id ? "selected" : @$item->id == @$data->product->inbox_to_id ? 'selected' : null}}>{{@$item->name}}</option>
                                        @endif
                                    @endforeach
                            </select>
                            @if ($errors->has('inbox_to_id'))
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('inbox_to_id') }}</strong>
                        </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="text" class="col-sm-3 control-label">Message:<span class="text-error">*</span></label>
                        <div class="col-sm-9">
                            <textarea id="text" class="form-control" name="inbox_text" required value=""></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <input type="submit" class="btn-default-1">
                        </div>
                    </div>
                </form>
            </div>
        </article>

    </section>
@endsection

