@extends('admin.template')
@section('title',  !empty($data->artikel) ? 'Edit' : 'Create')
@section('sub_title', 'Artikel')
@section('menu','Main Menu')
@section('content')
    <section>

        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="block-form box-border wow fadeInLeft animated" data-wow-duration="1s">
                <h3><i class="fa fa-tags"></i>Artikel Form</h3>
                <hr>
                <form class="form-horizontal" role="form" method="post" action="{{ !empty($data) ? route('admin.artikel.update', ['id' => $data->id_artikel]) : route('admin.artikel.post') }}" enctype="multipart/form-data">
                    @csrf
                   {{-- <input type="hidden" name="id_artikel" value="{{ $data->id_artikel }}">--}}
                    <div class="form-group">
                        <label for="title" class="col-sm-3 control-label">Artikel Name:<span class="text-error">*</span></label>
                        <div class="col-sm-9">
                            <input id="title" type="text" class="form-control{{ $errors->has('artikel_title') ? ' is-invalid' : '' }}" name="artikel_title" value="{{ @$data->artikel_title ? @$data->artikel_title :old('artikel_title') }}" required>
                            @if ($errors->has('artikel_title'))
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('artikel_title') }}</strong>
                        </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="title" class="col-sm-3 control-label">Artikel URL:<span class="text-error">*</span></label>
                        <div class="col-sm-9">
                            <input id="title" type="text" class="form-control{{ $errors->has('artikel_url') ? ' is-invalid' : '' }}" name="artikel_url" value="{{ @$data->artikel_url ? @$data->artikel_url :old('artikel_url') }}" required>
                            @if ($errors->has('artikel_url'))
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('artikel_url') }}</strong>
                        </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="desc" class="col-sm-3 control-label">Artikel Description:<span class="text-error">*</span></label>
                        <div class="col-sm-9">
                            <textarea id="desc" class="form-control" name="artikel_des" required>{{ @$data->artikel_des ? @$data->artikel_des :old('artikel_des') }} </textarea>
                            @if ($errors->has('artikel_des'))
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('artikel_des') }}</strong>
                        </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="file" class="col-sm-3 control-label">Artikel Image:<span class="text-error">*</span></label>
                        <div class="col-sm-9">
                        @if(@$data->artikel_image)
                            <img src="{{url($data->artikel_image)}}" alt="" id="img" height="120px">
                            <br>
                        @endif
                        <input id="file" type="file" name="file" class="form-control">
                        @if ($errors->has('file'))
                            <span class="help-block" style="color: red">
                                    <strong>{{ $errors->first('file') }}</strong>
                                </span>
                        @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn-default-1">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </article>

    </section>
@endsection

