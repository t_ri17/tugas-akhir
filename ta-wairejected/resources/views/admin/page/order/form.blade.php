@extends('admin.template')
@section('title',  'Detail')
@section('sub_title', 'Order')
@section('menu','Main Menu')
@section('content')
    <section>
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="block-form box-border wow fadeInLeft animated" data-wow-duration="1s">
                <h3><i class="fa fa-shopping-cart"></i>Detail Order</h3>
                <hr>
                    <table class="table table-bordered table-striped" cellspacing="0" width="100%">
                        <tbody>
                        <tr>
                            <th>User Name</th>
                            <td>{{ $data->order->name }}</td>
                        </tr>
                        <tr>
                            <th>Items</th>
                            <td>{{ $data->order->order_items }}</td>
                        </tr>
                        <tr>
                            <th>Total Price</th>
                            <td>{{ $data->order->order_price }}</td>
                        </tr>
                        <tr>
                            <th>Status</th>
                            <td>
                                <form class="form-horizontal" role="form" method="post" action="{{ route('admin.order.update', ['id' => $data->order->order_id]) }}">
                                    @csrf
                                        <div class="input-group">
                                            <select name="status" class="form-control">
                                                <option value="unpaid" {{ @$data->order->status === 'unpaid' ? 'selected' :null }}>Unpaid</option>
                                                <option value="paid" {{ @$data->order->status === 'paid' ? 'selected' :null }}>Paid</option>
                                                <option value="delivery" {{ @$data->order->status === 'delivery' ? 'selected' :null }}>Delivery</option>
                                                <option value="arrived" {{ @$data->order->status === 'arrived' ? 'selected' :null }}>Arrived</option>
                                            </select>
                                            <div class="input-group-btn"><button type="submit" class="btn btn-info">Change</button></div>
                                        </div>
                                </form>
                            </td>
                        </tr>
                        <tr>
                            <th>Transaction</th>
                            <td><img src="{{url($data->order->order_payment_img) }}" height="150px"></td>
                        </tr>
                        <tr>
                            <th>City</th>
                            <td>{{ $data->order->order_city }}</td>
                        </tr>
                        <tr>
                            <th>Address</th>
                            <td>{{ $data->order->order_address }}</td>
                        </tr>
                        <tr>
                            <th>Date Time</th>
                            <td>{{ $data->order->created_at }}</td>
                        </tr>
                        </tbody>
                    </table>

            </div>
        </article>
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="block-form box-border wow fadeInLeft animated" data-wow-duration="1s">
                <h3><i class="fa fa-shopping-cart"></i>Detail Items</h3>
                <hr>

                    <table class="table table-bordered table-striped" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Image</th>
                            <th>Qty</th>
                            <th>Price</th>
                            <th>Price Total</th>
                            <th>Tanggal</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data->od as $item)
                            <tr>
                                <td>{{$item->product_title}}</td>
                                <td><img src="{{ url($item->product_image)}}" alt="Smiley face" height="42" width="42"></td>
                                <td>{{$item->order_detail_qty}}</td>
                                <td>{{$item->product_price}}</td>
                                <td>{{$item->order_detail_price}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

            </div>
        </article>
    </section>
@endsection

