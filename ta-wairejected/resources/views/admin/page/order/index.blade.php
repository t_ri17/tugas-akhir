@extends('admin.template')
@section('title', 'Data')
@section('sub_title', 'Order')
@section('menu','Main Menu')
@section('content')
    <table id="table-layout" class="table table-bordered table-striped" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>User Name</th>
            <th>Items</th>
            <th>Total Price</th>
            <th>Status</th>
            <th>Transaction</th>
            <th>City</th>
            <th>Address</th>
            <th>Date Time</th>
            <th class="" style="text-align: center; width: 20%;"><span class="fa fa-ellipsis-v "></span></th>
        </tr>
        </thead>
    </table>
@endsection
@section('js')
    <script>
        var table = $('#table-layout').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('admin.order.get') }}",
            columns: [
                {data: 'name', name: 'name'},
                {data: 'order_items', name: 'order_items'},
                {data: 'order_price', name: 'order_price'},
                {data: 'status', name: 'status'},
                {data: 'img', name: 'img'},
                {data: 'order_city', name: 'order_city'},
                {data: 'order_address', name: 'order_address'},
                {data: 'created_at', name: 'created_at'},
                {data: 'action', name: 'action', searchable: false, orderable: false}
            ],
        });

        function isdelete(k) {
            swal({
                    title: "Are you sure?",
                    text: "Data Akan Terhapus!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#fb483a',
                    confirmButtonText: "Yes, delete it!"
                },
                function(){
                    $('#delete-form'+k).submit();
                });
        }
    </script>
@endsection
