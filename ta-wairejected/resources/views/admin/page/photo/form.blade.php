@extends('admin.template')
@section('title',  !empty($data->photo) ? 'Edit' : 'Create')
@section('sub_title', 'Photo')
@section('menu','Main Menu')
@section('content')
    <section>

        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="block-form box-border wow fadeInLeft animated" data-wow-duration="1s">
                <h3><i class="fa fa-tags"></i>Photo Form</h3>
                <hr>
                <form class="form-horizontal" role="form" method="post" action="{{ !empty($data) ? route('admin.photo.update', ['id' => $data->photo_id]) : route('admin.photo.post') }}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="photo_id" value="{{ $data->photo_id }}">
                    <div class="form-group">
                        <label for="title" class="col-sm-3 control-label">Photo Name:<span class="text-error">*</span></label>
                        <div class="col-sm-9">
                            <input id="title" type="text" class="form-control{{ $errors->has('photo_title') ? ' is-invalid' : '' }}" name="photo_title" value="{{ @$data->photo_title ? @$data->photo_title :old('photo_title') }}" required>
                            @if ($errors->has('photo_title'))
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('photo_title') }}</strong>
                        </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="file" class="col-sm-3 control-label">Photo Image:<span class="text-error">*</span></label>
                        <div class="col-sm-9">
                            @if(@$data->photo_image)
                                <img src="{{url($data->photo_image)}}" alt="" id="img" height="120px">
                                <br>
                            @endif
                            <input id="file" type="file" name="file" class="form-control">
                            @if ($errors->has('file'))
                                <span class="help-block" style="color: red">
                                    <strong>{{ $errors->first('file') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn-default-1">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </article>

    </section>
@endsection
