@extends('admin.template')
@section('title',  !empty($data) ? 'Edit' : 'Create')
@section('sub_title', 'Users')
@section('menu','Main Menu')
@section('content')
    <section>

        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="block-form box-border wow fadeInLeft animated" data-wow-duration="1s">
                <h3><i class="fa fa-tags"></i>User Form</h3>
                <hr>
                <form class="form-horizontal" role="form" method="post" action="{{ !empty($data) ? route('admin.user.update', ['id' => $data->id]) : route('admin.user.post') }}">
                    @csrf

                    <div class="form-group">
                        <label for="email" class="col-sm-3 control-label">Email:<span class="text-error">*</span></label>
                        <div class="col-sm-9">
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ @$data->email ? @$data->email :old('email') }}" required>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="user" class="col-sm-3 control-label">Name:<span class="text-error">*</span></label>
                        <div class="col-sm-9">
                            <input id="user" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ @$data->name ? @$data->name :old('name') }}" required>
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-sm-3 control-label">Password:<span class="text-error">*</span></label>
                        <div class="col-sm-9">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="">
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="confirm">Password Confirm:<span class="text-error">*</span></label>
                        <div class="col-sm-9">
                            <input type="password" name="password-confirm" class="form-control" id="confirm" {{!empty($data->user) ? null : ''}} value="{{ old('password-confirm') }}">
                            @if ($errors->has('password-confirm'))
                                <span class="help-block" style="color: red">
                                    <strong>{{ $errors->first('password-confirm') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="user_phone" class="col-sm-3 control-label">Phone:<span class="text-error">*</span></label>
                        <div class="col-sm-9">
                            <input id="user_phone" type="text" class="form-control{{ $errors->has('user_phone') ? ' is-invalid' : '' }}" name="user_phone" value="{{ @$data->user_phone ? @$data->user_phone :old('user_phone') }}" required>
                            @if ($errors->has('user_phone'))
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('user_phone') }}</strong>
                        </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="type" class="col-sm-3 control-label">User Level:<span class="text-error">*</span></label>
                        <div class="col-sm-9">
                            <select id="type" class="form-control">
                                <option value="user" {{ @$data->user_type === 'user' ? 'selected' :null }}>User</option>
                                <option value="admin" {{ @$data->user_type === 'admin' ? 'selected' :null }}>Admin</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn-default-1">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </article>
    </section>
@endsection

