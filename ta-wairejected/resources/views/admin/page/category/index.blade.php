@extends('admin.template')
@section('title', 'Data')
@section('sub_title', 'Category')
@section('menu','Main Menu')
@section('content')
    <table id="table-layout" class="table table-bordered table-striped" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>Category Name</th>
            <th class="" style="text-align: center; width: 20%;"><span class="fa fa-ellipsis-v "></span></th>
        </tr>
        </thead>
    </table>
@endsection
@section('js')
    <script>
        var table = $('#table-layout').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('admin.category.get') }}",
            columns: [
                {data: 'category_id', name: 'category_id'},
                {data: 'category_name', name: 'category_name'},
                {data: 'action', name: 'action', searchable: false, orderable: false}
            ],
        });

        function isdelete(k) {
            swal({
                    title: "Are you sure?",
                    text: "Data Akan Terhapus!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#fb483a',
                    confirmButtonText: "Yes, delete it!"
                },
                function(){
                    $('#delete-form'+k).submit();
                });
        }
    </script>
@endsection
