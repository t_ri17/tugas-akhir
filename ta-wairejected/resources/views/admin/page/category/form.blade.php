@extends('admin.template')
@section('title',  !empty($data) ? 'Edit' : 'Create')
@section('sub_title', 'Category')
@section('menu','Main Menu')
@section('content')
    <section>

        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="block-form box-border wow fadeInLeft animated" data-wow-duration="1s">
                <h3><i class="fa fa-tags"></i>Category Form</h3>
                <hr>

                <form class="form-horizontal" role="form" method="post" action="{{ !empty($data) ? route('admin.category.update', ['id' => $data->category_id]) : route('admin.category.post') }}">
                    @csrf
                    <div class="form-group">
                        <label for="category" class="col-sm-3 control-label">Category Name:<span class="text-error">*</span></label>
                        <div class="col-sm-9">
                            <input id="category" type="text" class="form-control{{ $errors->has('category_name') ? ' is-invalid' : '' }}" name="category_name" value="{{ @$data->category_name ? @$data->category_name :old('category_name') }}" required>
                            @if ($errors->has('category_name'))
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('category_name') }}</strong>
                        </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn-default-1">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </article>

    </section>
@endsection

