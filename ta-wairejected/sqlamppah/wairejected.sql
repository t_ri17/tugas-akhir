-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 15 Agu 2018 pada 13.52
-- Versi server: 10.1.34-MariaDB
-- Versi PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wairejected`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `categories`
--

CREATE TABLE `categories` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`, `created_at`, `updated_at`) VALUES
(8, 'T-SHIRT', NULL, NULL),
(9, 'BACKPACK', NULL, NULL),
(10, 'PANTS', NULL, NULL),
(11, 'HATS', NULL, NULL),
(12, 'SHIRT', NULL, NULL),
(13, 'JACKET', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `inboxes`
--

CREATE TABLE `inboxes` (
  `inbox_id` int(10) UNSIGNED NOT NULL,
  `inbox_from_id` int(11) NOT NULL,
  `inbox_to_id` int(11) NOT NULL,
  `inbox_text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `inboxes`
--

INSERT INTO `inboxes` (`inbox_id`, `inbox_from_id`, `inbox_to_id`, `inbox_text`, `created_at`, `updated_at`) VALUES
(2, 3, 1, 'asdasd', NULL, NULL),
(3, 1, 4, 'cok pe kabar', NULL, NULL),
(4, 1, 4, 'test test', NULL, NULL),
(12, 2, 1, 'steasd', NULL, NULL),
(13, 2, 1, 'asdasdasd', NULL, NULL),
(14, 1, 2, 'test user', NULL, NULL),
(15, 2, 9, 'halo lur', NULL, NULL),
(16, 9, 2, 'halo noob', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_08_08_172808_create_inboxes_table', 1),
(4, '2018_08_08_172941_create_categories_table', 1),
(5, '2018_08_08_173206_create_products_table', 1),
(6, '2018_08_08_173829_create_orders_table', 1),
(7, '2018_08_08_175332_create_order_details_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `orders`
--

CREATE TABLE `orders` (
  `order_id` int(10) UNSIGNED NOT NULL,
  `order_user_id` int(11) NOT NULL,
  `order_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_price` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_zip` int(11) NOT NULL,
  `order_items` int(4) DEFAULT NULL,
  `status` enum('unpaid','paid','delivery','arrived') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'unpaid',
  `order_payment_img` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `orders`
--

INSERT INTO `orders` (`order_id`, `order_user_id`, `order_address`, `order_price`, `order_city`, `order_zip`, `order_items`, `status`, `order_payment_img`, `created_at`, `updated_at`) VALUES
(13, 2, '123', '135000', '123', 123, 1, 'unpaid', NULL, '2018-08-15 01:12:18', NULL),
(14, 2, 'Jl. Parit Punk', '135000', 'Pontianak', 29839, 1, 'unpaid', NULL, '2018-08-15 02:47:18', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `order_details`
--

CREATE TABLE `order_details` (
  `order_detail_id` int(10) UNSIGNED NOT NULL,
  `order_detail_order_id` int(11) NOT NULL,
  `order_detail_product_id` int(11) NOT NULL,
  `order_detail_price` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_detail_qty` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `order_details`
--

INSERT INTO `order_details` (`order_detail_id`, `order_detail_order_id`, `order_detail_product_id`, `order_detail_price`, `order_detail_qty`, `created_at`, `updated_at`) VALUES
(9, 9, 12, '200000', 1, '2018-08-14 15:04:09', NULL),
(10, 10, 7, '135000', 1, '2018-08-14 15:22:47', NULL),
(11, 11, 7, '135000', 1, '2018-08-15 01:10:34', NULL),
(12, 13, 7, '135000', 1, '2018-08-15 01:12:18', NULL),
(13, 14, 7, '135000', 1, '2018-08-15 02:47:18', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('admin@email.com', '$2y$10$kRh4RjFWKkvJr.lVE0JIiuj2GAlgm6NtQa4Cq0kOYXBrzdc9/3Roe', '2018-08-13 07:23:01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `products`
--

CREATE TABLE `products` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `product_category_id` int(11) NOT NULL,
  `product_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_description` text COLLATE utf8mb4_unicode_ci,
  `product_price` double NOT NULL,
  `product_stock` int(11) NOT NULL,
  `product_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `products`
--

INSERT INTO `products` (`product_id`, `product_category_id`, `product_title`, `product_size`, `product_description`, `product_price`, `product_stock`, `product_image`, `created_at`, `updated_at`) VALUES
(7, 8, 'EAGLE WILD', 'M', 'Cotton comber 20s Rubber sablon', 135000, 1, 'product/2e18d81b79ab76cf1d52de868ec9df37.jpg', NULL, NULL),
(8, 10, 'BROWN CHINO', 'S', 'Material: 98% Cotton and 2% Spandex', 180000, 1, 'product/a2469785a938bb088722814163168d87.jpg', NULL, NULL),
(9, 9, 'OLD VINTAGE BACKPACK', 'All Size', 'Modernizes an iconic vintage style', 250000, 1, 'product/0fbcd8a888208d7a17dcdab500313f23.jpg', NULL, NULL),
(10, 11, 'WILD HATS', 'All Size', 'Material Cotton-Poly Nylon Mesh', 135000, 1, 'product/1e1bcc7d2fd2d969ffae9ddec217e631.jpg', NULL, NULL),
(11, 13, 'DARK ARMY PARKA', 'M', 'Material : 50% Cotton/50% Nylon.', 300000, 1, 'product/145aca78461c0060ce619dd42d4e9fcb.jpg', NULL, NULL),
(12, 12, 'NICE SHIRT GREEN', 'S', 'Material Cotton fleece 20s', 200000, 1, 'product/2b6cc5926c6f28c845e313875d545b74.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` enum('user','admin') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `user_phone` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `user_type`, `user_phone`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'user', 'user@email.com', 'user', NULL, '$2y$10$LeSeb2VVTlUbO758oWI4Wu.N/D6XstjTOr/PPSSzQdW7yh2cca.rW', 'YXF7rQ6EeETZaY9GmICRujN9AjP9KwZMP9FmlWJVvNNwl8guukar7R5cyskz', '2018-08-08 19:49:01', '2018-08-08 19:49:01'),
(3, 'Galang', '2202insane@gmail.com', 'user', NULL, '$2y$10$oyb6QMUtz9aK16yweS.sFuMv0bmaye6KlxwrEITzbH/x0dqgYo8m.', 'SrhbLTuZhhT0CNPh38rDRqFTFSiJsniDH6MECaHg1ZhyaEy7Kpp2Q7Tm4YM7', '2018-08-09 04:24:14', '2018-08-09 04:24:14'),
(5, 'kiki', 'kiki@email.com', 'user', '123123', '$2y$10$tw3zwGYWrz0GD8pDTuqyk.AazhIofL.FYkztnfU3nHuV/0kJanx7q', 'gyAHd4nLPvxaRViFudnL1cIoVueoV4n3A41p3YVv533bx40Ynh8LV2L2pIno', '2018-08-11 16:29:34', '2018-08-11 16:29:34'),
(7, 'adminuser', 'adminuser@email.com', 'user', '123123', '$2y$10$alCh3Tys5PuivF9H5UZRXOdrBMxCz1.ikR.qqdZ5GzFczEfTc8PIK', NULL, '2018-08-13 08:24:01', '2018-08-13 08:24:01'),
(9, 'admin', 'admin@email.com', 'admin', '123123', '$2y$10$LeSeb2VVTlUbO758oWI4Wu.N/D6XstjTOr/PPSSzQdW7yh2cca.rW', 'ihxHqVajxhdsMDqbSijfOr2Y71r8MTqdgebP3ruGePemq3zYV4kfELslHbiO', '2018-08-13 08:49:11', '2018-08-13 08:49:11');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`) USING BTREE;

--
-- Indeks untuk tabel `inboxes`
--
ALTER TABLE `inboxes`
  ADD PRIMARY KEY (`inbox_id`) USING BTREE;

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`) USING BTREE;

--
-- Indeks untuk tabel `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`order_detail_id`) USING BTREE;

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191)) USING BTREE;

--
-- Indeks untuk tabel `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`) USING BTREE;

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `users_email_unique` (`email`) USING BTREE;

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `inboxes`
--
ALTER TABLE `inboxes`
  MODIFY `inbox_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `order_details`
--
ALTER TABLE `order_details`
  MODIFY `order_detail_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
