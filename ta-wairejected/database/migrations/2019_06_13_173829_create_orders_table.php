<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('order_id');
            $table->integer('order_payment_id');
            $table->integer('order_user_id');
            $table->string('order_address');
            $table->double('order_price');
            $table->string('order_city');
            $table->integer('order_zip');
            $table->enum('status',['unpaid','paid','delivery','arrived'])->default('unpaid');;
            $table->text('order_payment_img')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
